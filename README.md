# BUMonitor and shelf_scan code

This code makes use of plugins that are either connections (read/writes to devices) or sensors (format and push sensor data)
It is broken up into several libraries and programs
- BUMonitor:
  Daemon program that monitors sensors via connections. 
- libBUMonitor:
  Base library for BUMonitor
- libBUMonitor_graphite:
  Plugin library for connection that talks to graphite
- libBUMonitor_ATCA:
  Plugin library with connections and sensors for shelves using freeipmi
- libBUMonitor_ApolloSM:
  Plugin library with connections and sensors for monitoring the ApolloSM blade
- shelf_scan
  A tool that can search a range of IPMB addresses and displays what is installed. 

## BUMonitor
This tool is controlled by a config file that specifies the plugins, connections, and sensors to use. 