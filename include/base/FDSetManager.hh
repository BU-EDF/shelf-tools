#ifndef __FDSETMANAGER_HH__
#define __FDSETMANAGER_HH__

#include <sys/select.h>

class FDSetManager{
public:
  FDSetManager(){};
  virtual ~FDSetManager(){};
  virtual void SetupFDSets(int &maxfd, fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){return;};
  virtual void ProcessFDSets(fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){return;};
};

#endif

