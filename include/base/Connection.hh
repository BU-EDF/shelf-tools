#ifndef __CONNECTION_HH__
#define __CONNECTION_HH__ 1

#include <base/Factory.hh>
#include <stdexcept>
#include <base/FDSetManager.hh>

class Connection : public FDSetManager{
public:
  Connection(std::string const & type,
	     std::string const & name);
  virtual ~Connection();
  virtual void Setup(std::vector<std::string> const &);
  virtual void DoConnect();
  virtual void Disconnect();
  virtual void SetupFDSets(int &maxfd, fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){return;};
  virtual void ProcessFDSets(fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){return;};
  std::string  GetName();
  std::string  GetType();
private:
  Connection();
  std::string name;
  std::string type;
};
#endif
