#ifndef __FACTORY_HH__
#define __FACTORY_HH__

//#include <base/Connection.hh>
//#include <base/Sensor.hh>

#include <vector>
#include <string>
#include <map>
#include <memory> //for smart pointer

class Connection;
class Sensor;

#define FUNCTION_PTR_SIGNATURE std::string const &type,                                   \
                               std::string const &name,					  \
                               std::vector<std::string> const & args

#define RegisterConnection(ClassName,ClassNickName)				                                  \
  namespace  {			                                          \
    /*make creator function*/						                                          \
    std::shared_ptr<Connection> connection_creator_function(FUNCTION_PTR_SIGNATURE){                              \
      std::shared_ptr<Connection> ret = std::static_pointer_cast<Connection>(std::make_shared<ClassName>(ClassName(type,name))); \
      ret->Setup(args);                                                                                           \
      return ret;		                                                                                  \
    }									                                          \
    /*Register the device with the DeviceFactory*/			                                          \
    const char connection_type[] = ClassNickName;					                          \
    _Pragma("GCC diagnostic push")	                                                                          \
    _Pragma("GCC diagnostic ignored \"-Wunused-variable\"")			                                  \
    const bool connection_registered = Factory::Instance()->RegisterConnectionType(connection_type,               \
										   &connection_creator_function); \
    _Pragma("GCC diagnostic pop")						                                  \
  }

#define RegisterSensor(ClassName,ClassNickName)				                              \
  namespace  {							      \
    /*make creator function*/						                              \
    std::shared_ptr<Sensor> sensor_creator_function(FUNCTION_PTR_SIGNATURE){                          \
      std::shared_ptr<Sensor> ret = std::static_pointer_cast<Sensor>(std::make_shared<ClassName>(ClassName(type,name))); \
    ret->Setup(args);                                                                                 \
    return ret;                                                                                       \
    }									                              \
    /*Register the device with the DeviceFactory*/			                              \
    const char sensor_type[] = ClassNickName;					                      \
    _Pragma("GCC diagnostic push")	                                                              \
    _Pragma("GCC diagnostic ignored \"-Wunused-variable\"")			                      \
    const bool sensor_registered = Factory::Instance()->RegisterSensorType(sensor_type,               \
									   &sensor_creator_function); \
    _Pragma("GCC diagnostic pop")						                      \
  }

class Factory{
public:
  //Singleton stuff
  static Factory * Instance() {
    if(NULL == pInstance){
      pInstance = new Factory;
    }
    return pInstance;
  }; 

  void SetVerbose(bool);
  
  //Sensors
  bool RegisterSensorType(std::string type,
			  std::shared_ptr<Sensor>         (*fPtr)(FUNCTION_PTR_SIGNATURE));
  std::shared_ptr<Sensor> CreateSensor(std::string ,
				       std::string ,
				       std::vector<std::string>);
  std::shared_ptr<Sensor> GetSensor(std::string);
  std::vector<std::string> GetSensorTypes();
  bool DeleteSensor(std::string);

  //Connections
  bool RegisterConnectionType(std::string type,
			      std::shared_ptr<Connection> (*fPtr)(FUNCTION_PTR_SIGNATURE));
  std::shared_ptr<Connection> CreateConnection(std::string,
					       std::string,
					       std::vector<std::string>);
  std::shared_ptr<Connection> GetConnection(std::string);
  std::vector<std::string> GetConnectionTypes();
  bool DeleteConnection(std::string);
  
private:  
  //Singleton stuff
  //Never implement
  Factory(){verbose=false;};
  Factory(const Factory &);
  Factory & operator=(const Factory &);
  
  ~Factory();
  static Factory * pInstance;

  bool verbose;
  
  //Sensor type List  
  std::map<std::string,std::shared_ptr<Sensor> (*)(FUNCTION_PTR_SIGNATURE)> sensorTypeMap;

  //Connection type List
  std::map<std::string,std::shared_ptr<Connection> (*)(FUNCTION_PTR_SIGNATURE)> connectionTypeMap;


  void InitConnection(std::shared_ptr<Connection>,
		      std::string const & type,
		      std::string const & name);
  void InitSensor(std::shared_ptr<Sensor>,
		  std::string const & type,
		  std::string const & name);

  //=========================================================
  // Memory managment
  //=========================================================

  //Sensors
  struct sSensor{
    std::string type;
    std::shared_ptr<Sensor> ptr;
  };
  std::map<std::string,sSensor> sensorMap;

  //Connections
  struct sConnection{
    std::string type;
    std::shared_ptr<Connection> ptr;
  };
  std::map<std::string,sConnection> connectionMap;



};

#endif
