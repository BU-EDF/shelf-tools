#ifndef __DB_CONNECTION_HH__
#define __DB_CONNECTION_HH__


#include <base/Connection.hh>
#include <base/net_helpers.hh>

class DBConnection : public Connection {
public:
  DBConnection(std::string const & type,
	       std::string const & name);
  ~DBConnection();

  virtual void Setup(std::vector<std::string> const &);
  virtual void DoConnect();
  virtual void Disconnect();

  virtual void Report(float,time_t time,std::string="");

  void SetDatabaseName(std::string const & name);
  std::string GetDatabaseName();
  
private:
  DBConnection();

//  std::string IP;
//  int port;  
//  int sockfd;
//  struct sockaddr_in servaddr;
//
//  size_t bufferSize;
//  char * buffer;

  std::string databaseName;
};

#endif
