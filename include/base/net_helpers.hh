#ifndef __net_helpers_hh__
#define __net_helpers_hh__
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <netinet/in.h>

ssize_t writen(int, const void *, size_t);

#endif
