#ifndef __SENSOR_HH__
#define __SENSOR_HH__

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <string>
#include <syslog.h>  

#include <memory> //for shared_ptr

#include <stdexcept>

//class Factory;
#include <base/Factory.hh>
#include <base/FDSetManager.hh>
#include <base/DBConnection.hh>

class Sensor : public FDSetManager{
public:
  Sensor(std::string const & type,
	 std::string const & name);
  virtual ~Sensor();

  virtual void Setup(std::vector<std::string> const &) {return;};
  virtual float GetVal();
  virtual int Report();
  virtual void SetupFDSets(int &maxfd, fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){return;};
  virtual void ProcessFDSets(fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){return;};
  std::string GetName();
  std::string GetType();
private:
  Sensor();
  std::string name;
  std::string type;

  std::shared_ptr<DBConnection> dbConn;

protected:  
  Factory* GetParent();
  void SetDBConnection(std::string const & );
  std::shared_ptr<DBConnection> GetDBConnection(){return dbConn;};
};

#endif
