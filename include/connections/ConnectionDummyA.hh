#ifndef __CONNECTION_DUMMYA_HH__
#define __CONNECTION_DUMMYA_HH__

#include <base/Connection.hh>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/inotify.h>
#include <limits.h>
#include <sys/time.h>

class ConnectionDummyA : public Connection {
public:
  ConnectionDummyA(std::string const & type,
		     std::string const & name);
  ~ConnectionDummyA();

  virtual void Setup(std::vector<std::string> const &);
  virtual void SetupFDSets(int &maxfd, fd_set * readFD, fd_set * writeFD, fd_set * exceptFD);
  virtual void ProcessFDSets(fd_set * readFD, fd_set * writeFD, fd_set * exceptFD);

private:
  std::string watchDirectory;
  int wd;
  int fd;
};


#endif
