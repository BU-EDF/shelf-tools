#ifndef __CONNECTION_GRAPHITE_HH__
#define __CONNECTION_GRAPHITE_HH__


#include <base/DBConnection.hh>
#include <base/net_helpers.hh>

class ConnectionGraphite : public DBConnection {
public:
  ConnectionGraphite(std::string const & type,
		     std::string const & name);
  ~ConnectionGraphite();

  virtual void Setup(std::vector<std::string> const &);
  virtual void DoConnect();
  virtual void Disconnect();
  virtual void SetupFDSets(int &maxfd, fd_set * readFD, fd_set * writeFD, fd_set * exceptFD);
  virtual void ProcessFDSets(fd_set * readFD, fd_set * writeFD, fd_set * exceptFD);

  void Report(float,time_t time,std::string);

  void SetDatabaseName(std::string const & name);
  std::string GetDatabaseName();
  
private:
  ConnectionGraphite();
  std::string IP;
  int port;  
  int sockfd;
  struct sockaddr_in servaddr;

  size_t bufferSize;
  char * buffer;

  std::string databaseName;
};

#endif
