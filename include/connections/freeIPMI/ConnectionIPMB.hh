#ifndef __CONNECTION_IPMB_HH__
#define __CONNECTION_IPMB_HH__

#include <base/Connection.hh>

#include <stdint.h>

#include <freeipmi/api/ipmi-api.h>
#include <freeipmi/spec/ipmi-authentication-type-spec.h>
#include <freeipmi/spec/ipmi-privilege-level-spec.h>


//Struct for parsing IPMB results
struct IPMBSensorResult {
  uint8_t empty_byte1;
  uint8_t empty_byte22;
  uint8_t sensorValue;
  uint8_t status;
  uint8_t mask;
};


class ConnectionIPMB : public Connection {
public:
  ConnectionIPMB(std::string const & type,
		 std::string const & name);
  virtual ~ConnectionIPMB();
  virtual void Setup(std::vector<std::string> const &);
  virtual void DoConnect();
  virtual void Disconnect();
  virtual void SetupFDSets(int &maxfd, fd_set * readFD, fd_set * writeFD, fd_set * exceptFD);
  virtual void ProcessFDSets(fd_set * readFD, fd_set * writeFD, fd_set * exceptFD);

  int rawCommand(uint8_t channelNumber,  // 0
		 uint8_t ipmbAddress,    //device address
		 uint8_t lun,            // 0 
		 uint8_t netFn,         // page 67 of ipmi doc.  0x4 is sensor/event
		 const void *requestBuffer,
		 unsigned int requestBufferSize, 
		 void *responseBuffer,
		 unsigned int responseBufferSize);
private:
  ConnectionIPMB();
  ipmi_ctx_t ipmiContext; //this is really a pointer
  bool setup;
  std::string hostname;
  std::string username;
  std::string password;

};


#endif
