#ifndef __CONNECTION_APOLLOSM_HH__
#define __CONNECTION_APOLLOSM_HH__

#include <base/Connection.hh>
#include <ApolloSM/ApolloSM.hh>
#include <sys/inotify.h>
#include <memory> //for shared_ptr
#include <sys/types.h> //for timestamp
#include <sys/stat.h> //for timestamp

class ConnectionApolloSM : public Connection {
public:
  ConnectionApolloSM(std::string const & type,
		     std::string const & name);
  ~ConnectionApolloSM();

  virtual void Setup(std::vector<std::string> const &);
  virtual void DoConnect();
  virtual void Disconnect();
  virtual void SetupFDSets(int &maxfd, fd_set * readFD, fd_set * writeFD, fd_set * exceptFD);
  virtual void ProcessFDSets(fd_set * readFD, fd_set * writeFD, fd_set * exceptFD);
  time_t getApolloSMTimestamp();
  std::shared_ptr<ApolloSM> const & GetSM();
  
private:
  ConnectionApolloSM();
  int wd;
  int fd;
  time_t ApolloSMTimestamp;
  std::string connectionFile;
  std::string watch_dir;
  std::string watch_file;
  std::shared_ptr<ApolloSM> SM;
};

#endif
