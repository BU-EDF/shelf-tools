#ifndef __CONNECTION_APOLLOSM_HH__
#define __CONNECTION_APOLLOSM_HH__

#include <base/Connection.hh>
#include <ApolloSM/ApolloSM.hh>


class ConnectionApolloSM : public Connection {
public:
  ConnectionApolloSM(std::string const & type,
		     std::string const & name);
  ~ConnectionApolloSM();

  virtual void Setup(std::vector<std::string> const &);
  virtual void DoConnect();
  virtual void Disconnect();

  ApolloSM const * GetSM();
  
private:
  ConnectionApolloSM();

  std::string connectionFile;
  ApolloSM * SM;
};

#endif
