#ifndef __APOLLO_MONITOR_HH__
#define __APOLLO_MONITOR_HH__
#include <base/Factory.hh>
#include <base/Sensor.hh>
#include <connections/ApolloSM/ConnectionApolloSM.hh>

class ApolloMonitor : public Sensor {

public:
  ApolloMonitor(std::string const & type,
		std::string const & name);
  void Setup(std::vector<std::string> const & args);
  virtual void SetupFDSets(int &maxfd, fd_set * readFD, fd_set * writeFD, fd_set * exceptFD);
  virtual void ProcessFDSets(fd_set * readFD, fd_set * writeFD, fd_set * exceptFD);
  virtual int Report();
  virtual float GetVal();
  void compareApolloSMTimestamps();
private:
  void ReplaceSN(std::string &);
  void ReplaceSN_HEX(std::string &);
  void ReplaceRN(std::string &);
  void ReplaceRN_HEX(std::string &);
  void ReplaceShelfID(std::string &);
  void ReplaceSlot(std::string &);
  void ReplaceSlot_HEX(std::string &);
  void ReplaceZynqIP(std::string &);
  void ReplaceIPMCIP(std::string &);

  void SetSensors();
  int ApolloSMTimestamp;
  std::shared_ptr<ConnectionApolloSM> connSM;
  std::string base;
  int level;
  std::string table;
};

RegisterSensor(ApolloMonitor,"ApolloMonitor")
#endif
