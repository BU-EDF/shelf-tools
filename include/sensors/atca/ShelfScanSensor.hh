#ifndef __SHELF_SCAN_SENSOR_HH__
#define __SHELF_SCAN_SENSOR_HH__

#include <base/Sensor.hh>
#include <connections/freeIPMI/ConnectionIPMB.hh>

#define FRUCount 0x7F

class ShelfScanSensor : public Sensor {

public:
  ShelfScanSensor(std::string const & type,
		 std::string const & name);
  virtual ~ShelfScanSensor();
  virtual void  Setup(std::vector<std::string> const &);
  virtual float GetVal();
  virtual int Report();
  virtual void SetupFDSets(int &maxfd, fd_set * readFD, fd_set * writeFD, fd_set * exceptFD);
  virtual void ProcessFDSets(fd_set * readFD, fd_set * writeFD, fd_set * exceptFD);
  
private:
  ShelfScanSensor();
  std::shared_ptr<ConnectionIPMB> shelfConn;
  enum FRUState {CONFIGURED=0x01,PRESENT=0x02};
  int8_t FRUExists[FRUCount];
};

//register the sensor
RegisterSensor(ShelfScanSensor,"ShelfScan")
#endif
