#ifndef __APOLLO_BLADE_HH__
#define __APOLLO_BLADE_HH__

#include <base/Sensor.hh>
#include <connections/freeIPMI/ConnectionIPMB.hh>
#include <vector>

class ApolloBlade : public Sensor {

public:
  ApolloBlade(std::string const & type,
	      std::string const & name);
  virtual ~ApolloBlade();
  virtual void  Setup(std::vector<std::string> const &);
  virtual int Report();
  virtual float GetVal();

private:
  ApolloBlade();
  std::vector<std::shared_ptr<Sensor> > apolloSensors;
  virtual void SetDatabaseName(std::string const &){};

};

//register the sensor
RegisterSensor(ApolloBlade,"ApolloBlade")
#endif
