#ifndef __IPMI_FAN_SPEED_SENSOR_HH__
#define __IPMI_FAN_SPEED_SENSOR_HH__

#include <base/Sensor.hh>
#include <connections/freeIPMI/ConnectionIPMB.hh>

class IPMIFanSpeedSensor : public Sensor {

public:
  IPMIFanSpeedSensor(std::string const & type,
		     std::string const & name);
  virtual ~IPMIFanSpeedSensor();
  virtual void  Setup(std::vector<std::string> const &);
  virtual float GetVal();
  virtual void SetupFDSets(int &maxfd, fd_set * readFD, fd_set * writeFD, fd_set * exceptFD);
  virtual void ProcessFDSets(fd_set * readFD, fd_set * writeFD, fd_set * exceptFD);
  
private:
  IPMIFanSpeedSensor();
  std::shared_ptr<ConnectionIPMB> shelfConn;

  int sensorNumber;
  uint8_t deviceAccessAddress;

};

//register the sensor
RegisterSensor(IPMIFanSpeedSensor,"IPMIFanSpeed")

#endif
