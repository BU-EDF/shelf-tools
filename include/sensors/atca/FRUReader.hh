#ifndef __FRU__READER__HH__
#define __FRU__READER__HH__

#include <string>
#include <vector>

#include <connections/freeIPMI/ConnectionIPMB.hh>

class FRUReader {

public:
  FRUReader(std::string const & connName,
	    uint8_t _deviceAddr,
	    int _fruID);

  int         GetFRUID(){return fruID;};
  uint8_t     GetAddr(){return deviceAddr;};
  
  time_t      GetBoardMfgDate();
  std::string GetBoardManufacturerName();
  std::string GetBoardProductName();
  std::string GetBoardSerialNumber();
  std::string GetBoardPartNumber();

  std::string GetProductManufacturerName();
  std::string GetProductName();
  std::string GetProductPartNumber();
  std::string GetProductVersionNumber();
  std::string GetProductSerialNumber();


  
  
private:
  FRUReader();
  
  void Read();
  int ReadFRUInventoryAreaInfo();
  int ReadCommonHeader();
  int ReadInternalUseArea();
  int ReadChassisInfoArea();
  std::vector<uint8_t> ChassisArea;
  int ReadBoardArea();
  std::vector<uint8_t> BoardArea;
  int ReadProductInfoArea();
  std::vector<uint8_t> ProductArea;
  int ReadMultRecordArea();

  int ReadArea(uint16_t offset,std::vector<uint8_t> & data);
  
  std::shared_ptr<ConnectionIPMB> conn;
  uint8_t deviceAddr;
  int fruID;
  
  int FRUInventorySize;
  #pragma pack(push,1)
  //pack this at the 1byte level since these are just for reading and that's how they are packed.
  //https://stackoverflow.com/questions/40642765/how-to-tell-gcc-to-disable-padding-inside-struct
  struct ReplyHeader{
    uint16_t padding;
    uint8_t completionCode;
  };
  struct {
    ReplyHeader rply;
    uint16_t size;
    uint8_t misc; //bit0  = 0 means bytes, 1 means words...
  } FRUInventoryAreaInfo;
  struct {
    ReplyHeader rply;    
    uint8_t version;
    uint8_t Offset_InternalUseArea;
    uint8_t Offset_ChassisInfoArea;
    uint8_t Offset_BoardArea;
    uint8_t Offset_ProductArea;
    uint8_t Offset_MultiRecordArea;
    uint8_t padding;
    uint8_t Checksum;
  } CommonHeader;
  #pragma pack(pop)


  
};
#endif
