#include <sensors/ApolloMonitor/ApolloMonitor.hh>
#include <base/net_helpers.hh>
#include <string>
#include <vector>
#include <stdexcept>

#include <syslog.h>

ApolloMonitor::ApolloMonitor(std::string const & type,
			     std::string const & name
			     ):
  Sensor(type,name){
  
}
void ApolloMonitor::Setup(std::vector<std::string> const & args){
  /*
  args:
    [0] Graphite connection name
    [1] connectionFile,
    [2] userBase, 
    [3] tableName, 
    [4] displayLevel
   */
  if(args.size() < 4){
    throw std::runtime_error("Too few arguments:\n "\
			     "args:\n"              \
			     " [0] Graphite connection name\n"\
			     " [1] ApolloSM connection name\n"\
			     " [2] userBase\n"\
			     " [3] tableName\n"\
			     " [4] displayLevel\n"
			     );
  }  

  //Setup db connection
  SetDBConnection(args[0]);

  //status display parameters
  level= atoi(args[4].c_str());
  table= args[3];

  //Get ApolloSM Connection
  try{
    connSM = std::dynamic_pointer_cast<ConnectionApolloSM>(GetParent()->GetConnection(args[1]));
  }
  catch (std::exception &e){
    syslog(LOG_INFO,"%s",e.what());
    return;
  }
  ApolloSMTimestamp = connSM->getApolloSMTimestamp();
  //Build base name using apollo SN
  base = args[2];
}

void ApolloMonitor::compareApolloSMTimestamps(){
  int newTimestamp = connSM->getApolloSMTimestamp();
  if(newTimestamp != ApolloSMTimestamp){
    ApolloSMTimestamp = connSM->getApolloSMTimestamp();
  }
  return;
}

int ApolloMonitor::Report() {
  std::string baseName = base;
  //Do any replacements we need to do
  ReplaceSN_HEX(baseName);//do first to match markup with an h
  ReplaceSN(baseName);
  ReplaceRN_HEX(baseName);
  ReplaceRN(baseName);
  ReplaceSlot_HEX(baseName);
  ReplaceSlot(baseName);
  ReplaceShelfID(baseName);
  ReplaceZynqIP(baseName);
  ReplaceIPMCIP(baseName);

  int sensorCount = 0;
  try{
    std::string sensorVals = connSM->GetSM()->GenerateGraphiteStatus(level,table);
    
    for(size_t iS = 0;
	iS < sensorVals.size();
	){
      //find the next "\n"
      size_t end = sensorVals.find_first_of("\n",iS);
      if(end == std::string::npos){
	break;
      }
      end++;

      //split the string
      std::istringstream iss(sensorVals.substr(iS,end));
      std::string entry;
      float value;
      time_t time = 0;
      iss >> entry;
      iss >> value;
      iss >> time;
      
      //Adding basename
      std::string dbAppend = baseName+"."+entry; 

      GetDBConnection()->Report(value,time,dbAppend);      

      //Write the data
      sensorCount++;
      iS = end;

      
      }	
  }catch(BUException::exBase const & e){
      syslog(LOG_ERR,"Caught BUException: %s\n   Info: %s\n",e.what(),e.Description());          
  }catch(std::exception const & e){
      syslog(LOG_ERR,"Caught std::exception: %s\n",e.what());          
  }
  return sensorCount;
}

float ApolloMonitor::GetVal(){
  return 0.0;
}

static std::string ReplaceInString(std::string original,
				   std::string const & replace,
				   std::string const & replacement){
  std::string ret;
  ret.clear();
  size_t lastPos = 0;
  size_t findPos = original.find(replace);
  while((findPos != std::string::npos) &&
	(findPos < original.size())){
    //append from the last position to this found position                                      
    ret.append(original.begin()+lastPos,
	       original.begin()+findPos);
    //append the replacement value
    ret.append(replacement);
    //move the last pointer, to the find pointer plus the replace size
    lastPos = findPos + replace.size();
    findPos = original.find(replace,lastPos);
  }
  //append the end of the string
  ret.append(original.begin() + lastPos,
	     original.begin() + original.size());
  return ret;
}                                                    

void ApolloMonitor::ReplaceSN(std::string & original){  
  char buffer[20];
  snprintf(buffer,20,"%02d",connSM->GetSM()->GetSerialNumber());
  original = ReplaceInString(original,"$SN",buffer);  
}
void ApolloMonitor::ReplaceSN_HEX(std::string & original){  
  char buffer[20];
  snprintf(buffer,20,"0x%02X",connSM->GetSM()->GetSerialNumber());
  original = ReplaceInString(original,"$SNh",buffer);  
}

void ApolloMonitor::ReplaceRN(std::string & original){  
  char buffer[20];
  snprintf(buffer,20,"%02d",connSM->GetSM()->GetRevNumber());
  original = ReplaceInString(original,"$RN",buffer);  
}
void ApolloMonitor::ReplaceRN_HEX(std::string & original){  
  char buffer[20];
  snprintf(buffer,20,"0x%02X",connSM->GetSM()->GetRevNumber());
  original = ReplaceInString(original,"$RNh",buffer);  
}

void ApolloMonitor::ReplaceShelfID(std::string & original){  
  std::string shelfID = connSM->GetSM()->GetShelfID();
  original = ReplaceInString(original,"$SHELF_ID",shelfID);  
}

void ApolloMonitor::ReplaceSlot(std::string & original){  
  char buffer[20];
  snprintf(buffer,20,"%02d",connSM->GetSM()->GetSlot());
  original = ReplaceInString(original,"$SLOT",buffer);  
}
void ApolloMonitor::ReplaceSlot_HEX(std::string & original){  
  char buffer[20];
  snprintf(buffer,20,"0x%02X",connSM->GetSM()->GetSlot());
  original = ReplaceInString(original,"$SLOTh",buffer);  
}

void ApolloMonitor::ReplaceZynqIP(std::string & original){  
  char buffer[20];
  snprintf(buffer,20,"%u-%u-%u-%u",  // "-" are because graphite splits on "."s
	   (connSM->GetSM()->GetZynqIP()>>24)&0xFF,
	   (connSM->GetSM()->GetZynqIP()>>16)&0xFF,
	   (connSM->GetSM()->GetZynqIP()>> 8)&0xFF,
	   (connSM->GetSM()->GetZynqIP()>> 0)&0xFF
	   );
  original = ReplaceInString(original,"$ZYNQ_IP",buffer);  
}

void ApolloMonitor::ReplaceIPMCIP(std::string & original){  
  char buffer[20];
  snprintf(buffer,20,"%u-%u-%u-%u", // "-" are because graphite splits on "."s
	   (connSM->GetSM()->GetIPMCIP()>>24)&0xFF,
	   (connSM->GetSM()->GetIPMCIP()>>16)&0xFF,
	   (connSM->GetSM()->GetIPMCIP()>> 8)&0xFF,
	   (connSM->GetSM()->GetIPMCIP()>> 0)&0xFF
	   );
  original = ReplaceInString(original,"$IPMC_IP",buffer);  
}

void ApolloMonitor::SetupFDSets(int &maxfd, fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){
  compareApolloSMTimestamps();
  return;
}

void ApolloMonitor::ProcessFDSets(fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){
  return;
}

