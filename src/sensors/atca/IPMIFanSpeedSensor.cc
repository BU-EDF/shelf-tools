#include <stdio.h>
#include <iostream>
#include <string.h>

#include <sensors/atca/IPMIFanSpeedSensor.hh>
#include <stdexcept>




IPMIFanSpeedSensor::IPMIFanSpeedSensor(std::string const & type,
				       std::string const & name):
  Sensor(type,name){
}

IPMIFanSpeedSensor::~IPMIFanSpeedSensor(){
}

void IPMIFanSpeedSensor::Setup(std::vector<std::string> const & args){
  /*
    args:    
      IPMI connection name
      Graphite connection name 
      sensorNum
      deviceAddr
  */
  if(args.size() < 4){
    throw std::runtime_error("Too few arguments:\n"              \
			     "  args:\n"	                 \
			     "    0  Graphite connection name\n" \
			     "    1  IPMI connection name\n"	\
			     "    2  sensorNum\n"                \
			     "    3  deviceAddr\n"
			     );
  }  


  // Setup graphite connection
  // This will fill the dbConn pointer inherited from the Sensor class.
  SetDBConnection(args[0]);
  
  //Setup the IPMB connection
  shelfConn = std::dynamic_pointer_cast<ConnectionIPMB>(GetParent()->GetConnection(args[1]));
  if(!shelfConn){
    throw std::runtime_error("Connection named "+args[1]+" exists, but can't be cast to ConnectionIPMB");    
  }

  GetDBConnection()->SetDatabaseName("Shelf."+shelfConn->GetName() + "." + GetName());


  // get the IPMB address of the device within the machine
  deviceAccessAddress = strtoul((char const *) args[3].c_str(), NULL, 0);

  //Get the sensor Number at this IPMB address
  sensorNumber = atoi(args[2].c_str());

  
}


float IPMIFanSpeedSensor::GetVal(){

  uint8_t channel_number = 0;
  uint8_t lun = 0;
  // net_fn code for "Sensor Events" 0x04
  uint8_t net_fn = 0x04;

  // it's necessary to send 2 raw bytes to get our sensor data back
  const size_t buf_rq_size = 2;
  uint8_t buf_rq[buf_rq_size];
  // 0x2d is the ipmi raw command for reading sensor values
  buf_rq[0] = 0x2d;
  // sensor number (235-238 for tray temps, 240-243 for fan speeds, etc)
  buf_rq[1] = sensorNumber;

  const size_t buf_rs_size = 256;
  uint8_t buf_rs[buf_rs_size];

  
  shelfConn->rawCommand (channel_number,
			 deviceAccessAddress,
			 lun,
			 net_fn,
			 (void const *) buf_rq, buf_rq_size,
			 buf_rs, buf_rs_size);
  
  
  IPMBSensorResult *ipmbSensorVal = (IPMBSensorResult *) buf_rs;
   
  if( ipmbSensorVal->status != 0xc0 ) {
    throw std::runtime_error("Event messages are disabled from this sensor\n");
  }
  
  if ( ipmbSensorVal->sensorValue < 0 ){
    throw std::range_error("Invalid sensor value\n");
  }

  float raw_fan_speed = float(ipmbSensorVal->sensorValue);
  //  float fan_speed_rpm = raw_fan_speed*46;
  int fan_speed_percent = raw_fan_speed/1.46;
  return fan_speed_percent;
}

void IPMIFanSpeedSensor::SetupFDSets(int &maxfdp, fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){
  return; 
}

void IPMIFanSpeedSensor::ProcessFDSets(fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){
  return;
}
