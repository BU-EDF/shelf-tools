#include <sensors/atca/FRUReader.hh>
#include <iostream>
#include <freeipmi/api/ipmi-api.h>
#include <freeipmi/spec/ipmi-authentication-type-spec.h>
#include <freeipmi/spec/ipmi-privilege-level-spec.h>
#include <stdexcept>
#include <string.h>
#include <string>

#include <base/Factory.hh>
#include <memory>

#define DevFac Factory::Instance()

FRUReader::FRUReader(std::string const & connName,
		     uint8_t _deviceAddr,
		     int _fruID):
  deviceAddr(_deviceAddr),
  fruID(_fruID){

  conn = std::dynamic_pointer_cast<ConnectionIPMB>(DevFac->GetConnection(connName));

  
  Read(); 
}

static void PrintHex(uint8_t const * data, int size){
  if(size < 0){
    return;
  }
  size_t usize = size;
  for(size_t i = 0;i < usize;){
    printf("0x%04X: ",uint16_t(i));
    std::string text;
    for(size_t j = 0; (j < 8) && (j < usize);j++){
      printf("%02X",data[i]);
      if((data[i] > 31) && (data[i] < 128)){
	text.push_back(data[i]);
      }else{
	text.push_back('-');
      }
      i++;
    }
    printf("  %s\n",text.c_str());
  }
}
//static void PrintHex(std::vector<uint8_t> const & data){
//  PrintHex(data.data(),data.size());
//}


void FRUReader::Read(){

  if(ReadFRUInventoryAreaInfo() < 0){
    throw std::runtime_error("Unable to read FRU Inventory Area Info");  
  }
  //FRUInventoryAreaInfo is now set
  if(ReadCommonHeader() < 0){
    throw std::runtime_error("Unable to read FRU Common header");  
  }
  //CommonHeader is now set
//  printf("version:                %02X\n",CommonHeader.version);
//  printf("Offset_InternalUseArea: %02X\n",CommonHeader.Offset_InternalUseArea);
//  printf("Offset_ChassisInfoArea: %02X\n",CommonHeader.Offset_ChassisInfoArea);
//  printf("Offset_BoardArea:       %02X\n",CommonHeader.Offset_BoardArea);
//  printf("Offset_ProductArea:     %02X\n",CommonHeader.Offset_ProductArea);
//  printf("Offset_MultiRecordArea: %02X\n",CommonHeader.Offset_MultiRecordArea);
  
  
  if(ReadInternalUseArea() < 0){
    throw std::runtime_error("Unable to read InternalUseArea ");  
  }
  
  if(ReadChassisInfoArea() < 0){
    throw std::runtime_error("Unable to read ChassisInfo");  
  }

  if(ReadBoardArea() < 0){
    throw std::runtime_error("Unable to read Board");  
  }
  
  if(ReadProductInfoArea() < 0){
    throw std::runtime_error("Unable to read ProductInfo");  
  }
  
}


int FRUReader::ReadArea(uint16_t offset,std::vector<uint8_t> & data){

  #pragma pack(push,1)
  //Force byte packing
  struct {
    uint8_t  cmd;
    uint8_t  fruID;
    uint16_t offset;
    uint8_t  size;
  } IPMIRead;
  #pragma pack(pop)


  IPMIRead.cmd    = 0x11; //Read FRU Data Command
  IPMIRead.fruID  = fruID;
  IPMIRead.offset = offset;
  IPMIRead.size   = 2;

  const size_t readSize = 16;
  const size_t buf_rs_size = readSize + sizeof(ReplyHeader);
  uint8_t buf_rs[buf_rs_size];

  int raw_result = conn->rawCommand(0, //channel number
				    deviceAddr,
				    0, //lun
				    0xa,  //0xa is net_fn for storage
				    (void const *) &IPMIRead, sizeof(IPMIRead),
				    (uint8_t *) buf_rs,
				    IPMIRead.size + sizeof(ReplyHeader));
  if (raw_result < 0){
    printf("ReadArea size %d %u\n",raw_result,IPMIRead.size);
    PrintHex(buf_rs,raw_result);
    return -1;
  }
  data.clear(); //clear any existing data in BoardArea
  //Copy the new data in the board area vector
  for(size_t iRead = 0;iRead < IPMIRead.size;iRead++){
    data.push_back(buf_rs[sizeof(ReplyHeader)+iRead]);
    IPMIRead.offset++; //Move the read offset
  }

  uint16_t dataToRead = data.back() * 8; //Byte 1 of the array is the total size in multiples of 8  bytes
  while(dataToRead){

    IPMIRead.size = (dataToRead < readSize) ? dataToRead : readSize;
    int raw_result = conn->rawCommand(0, //channel number
				      deviceAddr,
				      0, //lun
				      0xa,  //0xa is net_fn for storage
				      (void const *) &IPMIRead, sizeof(IPMIRead),
				      (uint8_t *) buf_rs,
				      IPMIRead.size + sizeof(ReplyHeader));
    //    if (raw_result != int(IPMIRead.size + sizeof(ReplyHeader))){
    if (raw_result < 0){
      printf("ReadArea size %d %d\n",raw_result,int(IPMIRead.size + sizeof(ReplyHeader)));
      PrintHex(buf_rs,raw_result);
      return -1;
    }

    //Copy the new data in the board area vector
    for(size_t iRead = 0;iRead < IPMIRead.size;iRead++){
      data.push_back(buf_rs[sizeof(ReplyHeader)+iRead]);
    }
    IPMIRead.offset += IPMIRead.size;
    dataToRead -= IPMIRead.size;

  }
  return data.size();  
}




int FRUReader::ReadFRUInventoryAreaInfo() {
  const size_t buf_rq_size = 2;
  uint8_t buf_rq[buf_rq_size];

  buf_rq[0] = 0x10;// Get FRU Inventory Area Info
  buf_rq[1] = fruID;


  int raw_result = conn->rawCommand(0, //channel number
				    deviceAddr,
				    0, //lun
				    0xa,  //0xa is net_fn for storage
				    (void const *) buf_rq, buf_rq_size,
				    (uint8_t *) &FRUInventoryAreaInfo,
				    sizeof(FRUInventoryAreaInfo));
  if (raw_result < 0){
    return -1;
  }
  return 0;
}

int FRUReader::ReadCommonHeader() {
  const size_t buf_rq_size = 5;
  uint8_t buf_rq[buf_rq_size];
  buf_rq[0] = 0x11; //Read FRU Data Command
  buf_rq[1] = fruID;
  buf_rq[2] = 0; //read offset lsb
  buf_rq[3] = 0; //read offset MSB
  buf_rq[4] = sizeof(CommonHeader) - sizeof(ReplyHeader);

  int raw_result = conn->rawCommand(0, //channel number
				    deviceAddr,
				    0, //lun
				    0xa,  //0xa is net_fn for storage
				    (void const *) buf_rq, buf_rq_size,
				    (uint8_t *) &CommonHeader,
				    sizeof(CommonHeader));
  if (raw_result < 0){
    return -1;
  }
  return 0;
}




int FRUReader::ReadInternalUseArea(){
  //This is proprietary and not worth reading out at this time
  return 0;
}     

int FRUReader::ReadChassisInfoArea(){
  if(ReadArea(CommonHeader.Offset_ChassisInfoArea*8,
	      ChassisArea) < 0){
    return -1;
  }  
  return 0;
}

int FRUReader::ReadBoardArea(){
  if(ReadArea(CommonHeader.Offset_BoardArea*8,
	      BoardArea) < 0){
    return -1;
  }  
  return 0;
}
int FRUReader::ReadProductInfoArea(){
  if(ReadArea(CommonHeader.Offset_ProductArea*8,
	      ProductArea) < 0){
    return -1;
  }  
  return 0;
}
int FRUReader::ReadMultRecordArea(){
  return 0;
}







time_t FRUReader::GetBoardMfgDate(){  
  time_t ret = BoardArea[3] + 0x100*BoardArea[4] + 0x10000*BoardArea[5]; //Minutes since 1996
  ret*=60; //Convert to seconds
  if(ret == 0){
    return 0;
  }
  ret += 820472400;   //Add time of 1996
  return ret;
}

static void cleanString(std::string & str){
  for(size_t pos = 0;
      pos < str.size();
      pos++){
    if(str[pos] == 0xa){
      //nothing
    }else if(!(str[pos]&0x1F)){
      str.erase(pos,1);
      pos--;
    }
  }
  str.push_back('\0');
}

std::string FRUReader::GetBoardManufacturerName(){
  std::string ret;
  ret.assign((char *)(&BoardArea[7]),BoardArea[6]&0x3f);
  cleanString(ret);
  return ret;
}
std::string FRUReader::GetBoardProductName(){
  size_t offset = 6 + (BoardArea[6]&0x3f) + 1; //ManufacturerName_pos + ManufacturerSize + 1
  std::string ret;
  ret.assign((char *)(&BoardArea[offset+1]),BoardArea[offset]&0x3f);
  cleanString(ret);
  return ret;
}

std::string FRUReader::GetBoardSerialNumber(){
  size_t offset = 6 + (BoardArea[6]&0x3f) + 1; //ManufacturerName_pos + ManufacturerSize + 1
  offset += (BoardArea[offset]&0x3f) + 1;      //ProductNameSize + 1
  std::string ret;
  ret.assign((char *)(&BoardArea[offset+1]),BoardArea[offset]&0x3f);
  cleanString(ret);
  return ret;
}
std::string FRUReader::GetBoardPartNumber(){
  size_t offset = 6 + (BoardArea[6]&0x3f) + 1; //ManufacturerName_pos + ManufacturerSize + 1
  offset += (BoardArea[offset]&0x3f) + 1;      //ProductNameSize + 1
  offset += (BoardArea[offset]&0x3f) + 1;      //SerialNumberSize + 1
  std::string ret;
  ret.assign((char *)(&BoardArea[offset+1]),BoardArea[offset]&0x3f);
  cleanString(ret);
  return ret;  
}


std::string FRUReader::GetProductManufacturerName(){
  std::string ret;
  ret.assign((char *)(&ProductArea[4]),ProductArea[3]&0x3f);
  cleanString(ret);
  return ret;
}
std::string FRUReader::GetProductName(){
  size_t offset = 3 + (ProductArea[3]&0x3f) + 1; //ManufacturerName_pos + ManufacturerSize + 1
  std::string ret;
  ret.assign((char *)(&ProductArea[offset+1]),ProductArea[offset]&0x3f);
  cleanString(ret);
  return ret;
}

std::string FRUReader::GetProductPartNumber(){
  size_t offset = 3 + (ProductArea[3]&0x3f) + 1; //ManufacturerName_pos + ManufacturerSize + 1
  offset += (ProductArea[offset]&0x3f) + 1;      //ProductNameSize + 1
  std::string ret;
  ret.assign((char *)(&ProductArea[offset+1]),ProductArea[offset]&0x3f);
  cleanString(ret);
  return ret;  
}

std::string FRUReader::GetProductVersionNumber(){
  size_t offset = 3 + (ProductArea[3]&0x3f) + 1; //ManufacturerName_pos + ManufacturerSize + 1
  offset += (ProductArea[offset]&0x3f) + 1;      //ProductNameSize + 1
  offset += (ProductArea[offset]&0x3f) + 1;      //PartNumberSize + 1
  std::string ret;
  ret.assign((char *)(&ProductArea[offset+1]),ProductArea[offset]&0x3f);
  cleanString(ret);
  return ret;
}

std::string FRUReader::GetProductSerialNumber(){
  size_t offset = 3 + (ProductArea[3]&0x3f) + 1; //ManufacturerName_pos + ManufacturerSize + 1
  offset += (ProductArea[offset]&0x3f) + 1;      //ProductNameSize + 1
  offset += (ProductArea[offset]&0x3f) + 1;      //PartNumberSize + 1
  offset += (ProductArea[offset]&0x3f) + 1;      //VersionNumberSize + 1
  std::string ret;
  ret.assign((char *)(&ProductArea[offset+1]),ProductArea[offset]&0x3f);
  cleanString(ret);
  return ret;
}
