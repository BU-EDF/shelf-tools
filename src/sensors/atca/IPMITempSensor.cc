#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <freeipmi/api/ipmi-api.h>
#include <freeipmi/spec/ipmi-authentication-type-spec.h>
#include <freeipmi/spec/ipmi-privilege-level-spec.h>
#include <sensors/atca/IPMITempSensor.hh>
#include <sensors/atca/IPMBSensorResult.hh>
#include <stdexcept>
#include <string.h>

IPMITempSensor::IPMITempSensor(std::string const & type,
			       std::string const & name
			       ):
  Sensor(type,name){
}
IPMITempSensor::~IPMITempSensor(){
}
void IPMITempSensor::Setup(std::vector<std::string> const & args){
  if(args.size() < 4){
    throw std::runtime_error("Too few arguments:\n"              \
			     "  args:\n"	                 \
			     "    0  Graphite connection name\n" \
			     "    1  IPMI connection name\n"	 \
			     "    2  sensorNum\n"                \
			     "    3  deviceAddr\n"               \
			     "    4  Override db name\n"
			     );
  }  

  //Setup db connection
  SetDBConnection(args[0]);

  //Setup the IPMB connection
  shelfConn = std::dynamic_pointer_cast<ConnectionIPMB>(GetParent()->GetConnection(args[1]));
  if(!shelfConn){
    throw std::runtime_error("Connection named "+args[1]+" exists, but can't be cast to ConnectionIPMB");    
  }

  if(args.size() >=5){
    GetDBConnection()->SetDatabaseName(args[4]);
  }else{
    GetDBConnection()->SetDatabaseName("Shelf."+shelfConn->GetName() + "." + GetName());
  }


  // get the IPMB address of the device within the machine
  deviceAccessAddress = strtoul((char const *) args[3].c_str(), NULL, 0);

  //Get the sensor Number at this IPMB address
  sensorNumber = atoi(args[2].c_str());
}


float IPMITempSensor::GetVal(){

  uint8_t channel_number = 0;
  uint8_t lun = 0;
  // net_fn code for "Sensor Events" 0x04
  uint8_t net_fn = 0x04;

  // it's necessary to send 2 raw bytes to get our sensor data back
  const size_t buf_rq_size = 2;
  uint8_t buf_rq[buf_rq_size];
  // 0x2d is the ipmi raw command for reading sensor values
  buf_rq[0] = 0x2d;
  // sensor number (235-238 for tray temps, 240-243 for fan speeds, etc)
  buf_rq[1] = sensorNumber;

  const size_t buf_rs_size = 256;
  uint8_t buf_rs[buf_rs_size];

  
  shelfConn->rawCommand(channel_number,
			deviceAccessAddress,
			lun,
			net_fn,
			(void const *) buf_rq, buf_rq_size,
			buf_rs, buf_rs_size);
  
  
  IPMBSensorResult *ipmbSensorVal = (IPMBSensorResult *) buf_rs;
   
  if( ipmbSensorVal->status != 0xc0 ) {
    throw std::runtime_error("Event messages are disabled from this sensor\n");
  }
  
  if ( ipmbSensorVal->sensorValue < 0 ){
    throw std::range_error("Invalid sensor value\n");
  }

  // the 3rd byte of output is the useful one
  return float(ipmbSensorVal->sensorValue);
}

void IPMITempSensor::SetupFDSets(int &maxfd, fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){
  return; 
}

void IPMITempSensor::ProcessFDSets(fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){
  return;
}
