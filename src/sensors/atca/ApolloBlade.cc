#include <sensors/atca/ApolloBlade.hh>
#include <stdexcept>


ApolloBlade::ApolloBlade(std::string const & type,
			 std::string const & name):
  Sensor(type,name){
}
ApolloBlade::~ApolloBlade(){
}

void ApolloBlade::Setup(std::vector<std::string> const & args){
  /*
    args:    
      IPMI connection name
      Graphite connection name 
  */
  if(args.size() < 3){
    throw std::runtime_error("Too few arguments:\n"              \
			     "  args:\n"	                 \
			     "    0  Graphite connection name\n" \
			     "    1  IPMI connection name\n"	 \
			     "    2  deviceAddr\n"               \
			     "    3  module number\n"            \
			     );
  }
  
  //build a vector of arguments
  std::vector<std::string> createArgs(5,"");
  createArgs[0] = args[0];
  createArgs[1] = args[1];
  createArgs[3] = args[2];
  createArgs[4] = "Blades.Apollo" + args[3] + ".";

  std::vector<std::pair<std::string,std::string> > sensorNames;
  sensorNames.push_back(std::pair<std::string,std::string>("3",  "InternalTemp"));
  sensorNames.push_back(std::pair<std::string,std::string>("4",  "Top"));
  sensorNames.push_back(std::pair<std::string,std::string>("5",  "Bottom"));
  sensorNames.push_back(std::pair<std::string,std::string>("6",  "Middle"));
  sensorNames.push_back(std::pair<std::string,std::string>("7",  "CommandModuleMicroController"));
  sensorNames.push_back(std::pair<std::string,std::string>("8",  "Firefly"));
  sensorNames.push_back(std::pair<std::string,std::string>("9",  "CommandModuleFPGA"));
  sensorNames.push_back(std::pair<std::string,std::string>("10", "CommandModuleRegulator"));
  sensorNames.push_back(std::pair<std::string,std::string>("11", "CommandModuleMCU"));
  sensorNames.push_back(std::pair<std::string,std::string>("12", "CommandModuleFPGAVU"));
  sensorNames.push_back(std::pair<std::string,std::string>("13", "CommandModuleFPGAKU"));
  sensorNames.push_back(std::pair<std::string,std::string>("14", "Firefly2"));
  sensorNames.push_back(std::pair<std::string,std::string>("15", "CommandModuleRegulator2"));

  for(size_t iSensor = 0; iSensor < sensorNames.size();iSensor++){
    createArgs[2] = sensorNames[iSensor].first;
    createArgs[4] += sensorNames[iSensor].second;

    std::shared_ptr<Sensor> sensor = Factory::Instance()->CreateSensor("IPMITemp",sensorNames[iSensor].second,createArgs);
    if(!sensor){
      apolloSensors.push_back(sensor);
    }
  }

  
}



int ApolloBlade::Report() {
  for(size_t i = 0; i < apolloSensors.size(); i++){
    try {
      apolloSensors[i]->Report();
    } catch (std::runtime_error &e) {
      
    }
  }
  return 0;
}

float ApolloBlade::GetVal(){
  return 0.0;
}
