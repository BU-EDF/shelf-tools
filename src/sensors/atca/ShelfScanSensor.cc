#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <freeipmi/api/ipmi-api.h>
#include <freeipmi/spec/ipmi-authentication-type-spec.h>
#include <freeipmi/spec/ipmi-privilege-level-spec.h>
#include <sensors/atca/ShelfScanSensor.hh>
#include <sensors/atca/IPMBSensorResult.hh>
#include <stdexcept>
#include <string.h>
#include <sstream>   
#include <cmath>  //For NAN
#include <ctime>  //For time

#include <iomanip> //because std streams suck

ShelfScanSensor::ShelfScanSensor(std::string const & type,
			       std::string const & name):
  Sensor(type,name){
  memset(FRUExists,0,sizeof(int8_t)*FRUCount);
}

ShelfScanSensor::~ShelfScanSensor(){
//  //Mark all FRUs as None now that we are shutting down
//  for(uint8_t device = 0x0;
//      device <= FRUCount;
//      device++){
//    std::stringstream ss;
//    ss.setf(std::ios::hex, std::ios::basefield);
//    ss.setf(std::ios::showbase);
//    ss << GetDatabaseName()
//       << "FRU." 
//       << (device << 1)
//       << ".";    
//    GetGraphiteConnection()->Report(ss.str()+"Present",NAN,0);
//  }
//
}

void ShelfScanSensor::Setup(std::vector<std::string> const & args){
  /*
    args:    
      IPMI connection name
      Graphite connection name 
  */
  if(args.size() < 2){
    throw std::runtime_error("Too few arguments:\n"              \
			     "  args:\n"	                 \
			     "    0  Graphite connection name\n" \
			     "    1  IPMI connection name\n"	 
			     );
  }  

  // Setup graphite connection
  // This will fill the dbConn pointer inherited from the Sensor class.
  SetDBConnection(args[0]);

  //Setup the IPMB connection
  shelfConn = std::dynamic_pointer_cast<ConnectionIPMB>(GetParent()->GetConnection(args[1]));
  if(!shelfConn){
    throw std::runtime_error("Connection named "+args[1]+" exists, but can't be cast to ConnectionIPMB");    
  }

  GetDBConnection()->SetDatabaseName("Shelf." + shelfConn->GetName() + ".");

}

float ShelfScanSensor::GetVal(){
  return -1;
}

int ShelfScanSensor::Report(){


  /*CHECK IF databaseName.c_str() is empty (check std::string docs) ,return -1 if so*/
  if (GetDBConnection()->GetDatabaseName().empty()){
    syslog(LOG_INFO,"database name is empty! Please update it before calling this function");
    return -1;
  }
  if(!GetDBConnection()){
    throw std::runtime_error("Missing Graphite connection "+
			     GetName()+
			     "/"+
			     GetType());
  }


  uint8_t channel_number = 0;
  uint8_t lun = 0;
  // net_fn code for "Application" 0x06
  uint8_t net_fn = 0x06;

  // it's necessary to send 2 raw bytes to get our sensor data back
  const size_t buf_rq_size = 1;
  uint8_t buf_rq[buf_rq_size];
  // 0x01 is the ipmi raw command for get device ID
  buf_rq[0] = 0x01;

  const size_t buf_rs_size = 256;
  uint8_t buf_rs[buf_rs_size];
  
  for(uint8_t device = 0x0;
      device <= FRUCount;
      device++){
    int rawRet = shelfConn->rawCommand(channel_number,
				       device<<1,
				       lun,
				       net_fn,
				       (void const *) buf_rq, buf_rq_size,
				       buf_rs, buf_rs_size);
    
    std::stringstream ss;
    ss << GetDBConnection()->GetDatabaseName()
       << "FRU.0x" 
       << std::hex << std::setfill('0') << std::setw(2)
       << (device << 1)
       << ".";
    
    bool FRUPresent = (rawRet>=0);
    //    bool FRUPresent = (rawRet>=3) && (buf_rs[0] == 0 && buf_rs[3] == 0 && buf_rs[2] == 0 );

    // Get timestamp
    time_t timeNow = time(NULL);

    // Entry name for Graphite
    std::string entryName = ss.str() + "Present";

    // FRUPresent is 1 when present, 0 when not
    GetDBConnection()->Report(FRUPresent, timeNow, entryName);

//    if(!(FRUExists[device]&FRUState::CONFIGURED)){
//      printf("Configuring FRU %d (%s)\n",device<<1,FRUPresent? "present":"empty");
//      GetGraphiteConnection()->Report(ss.str()+"Present",
//				      FRUPresent,                        //1 when present, 0 when not
//				      0);
//      FRUExists[device] |= FRUState::CONFIGURED;
//    }else{
//      if( (!!(FRUExists[device]&FRUState::PRESENT)) != FRUPresent){
//      	printf("Updating FRU %d\n",device<<1);
//	GetGraphiteConnection()->Report(ss.str()+"Present",
//					FRUPresent,                        //1 when present, 0 when not
//					0);
//
//      }
  

//    FRUExists[device] = (FRUExists[device] & (~FRUState::PRESENT)) | (FRUState::PRESENT * FRUPresent);
    
    ss.str().clear();
  }
  return 1;
}

void ShelfScanSensor::SetupFDSets(int &maxfd, fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){
  return; 
}

void ShelfScanSensor::ProcessFDSets(fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){
  return;
}
