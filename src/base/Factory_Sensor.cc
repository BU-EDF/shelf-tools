#include <base/Factory.hh>
#include <boost/algorithm/string/case_conv.hpp>


bool Factory::RegisterSensorType(std::string type, 
				 std::shared_ptr<Sensor> (*fPtr)(FUNCTION_PTR_SIGNATURE)){
  boost::algorithm::to_upper(type);
  auto it = sensorTypeMap.find(type);
  
  if(it == sensorTypeMap.end()){
    //Register a device for the first time
    sensorTypeMap[type] = fPtr;
    if(verbose){
      printf("Registered Sensor: %s\n",type.c_str());
    }
  }else{
    //This type is already registered.
    //This can happen since multiple files can include the header. 
  }

  //Register command line options
  return true;
}

std::shared_ptr<Sensor> Factory::CreateSensor(std::string type ,
					      std::string name,
					      std::vector<std::string> args){
  boost::algorithm::to_upper(type);
  auto itType = sensorTypeMap.find(type);
  if(itType == sensorTypeMap.end()){
    throw std::runtime_error("Unkown Sensor type");
  }

  boost::algorithm::to_upper(name);
  //  std::map<std::string,sSensor>::iterator itName = sensorMap.find(name);
  auto itName = sensorMap.find(name);
  if(itName != sensorMap.end()){
    throw std::runtime_error("Duplicate sensor name");
  }
  printf("Creating sensor %s(%s) with args:",name.c_str(),type.c_str());
  for(size_t i=0;i<args.size();i++){
    printf(" %s",args[i].c_str());
  }
  printf("\n");
  std::shared_ptr<Sensor> ret = (*itType->second)(type,name,args);
  sSensor newSensor {type,ret};
  sensorMap[name]=newSensor;
  return ret;
}

std::shared_ptr<Sensor> Factory::GetSensor(std::string name){
  boost::algorithm::to_upper(name);
  std::map<std::string,sSensor>::iterator itName = sensorMap.find(name);
  if(itName != sensorMap.end()){
    return itName->second.ptr;
  }

  throw std::runtime_error("Sensor not found");      
  //never called
  return std::shared_ptr<Sensor>();
}


bool Factory::DeleteSensor(std::string name){
  boost::algorithm::to_upper(name);
  std::map<std::string,sSensor>::iterator itName = sensorMap.find(name);
  if(itName != sensorMap.end()){
    sensorMap.erase(itName);
    return true;
  }

  return false;
}


std::vector<std::string> Factory::GetSensorTypes(){
  std::vector<std::string> ret;
  for(auto it = sensorTypeMap.begin();
      it != sensorTypeMap.end();
      ++it){
    ret.push_back(it->first);
  }
  return ret;
}
