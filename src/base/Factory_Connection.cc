#include <base/Factory.hh>
#include <boost/algorithm/string/case_conv.hpp>


bool Factory::RegisterConnectionType(std::string type, 
				     std::shared_ptr<Connection> (*fPtr)(FUNCTION_PTR_SIGNATURE)){
  boost::algorithm::to_upper(type);
  auto it = connectionTypeMap.find(type);
  
  if(it == connectionTypeMap.end()){
    //Register a device for the first time
    connectionTypeMap[type] = fPtr;
    if(verbose){
      printf("Registered Connection: %s\n",type.c_str());
    }
  }else{
    //This type is already registered.
    //This can happen since multiple files can include the header. 
  }

  //Register command line options
  return true;
}

std::shared_ptr<Connection> Factory::CreateConnection(std::string type ,
						      std::string name,
						      std::vector<std::string> args){
  boost::algorithm::to_upper(type);
  auto itType = connectionTypeMap.find(type);
  if(itType == connectionTypeMap.end()){
    throw std::runtime_error("Unkown Connection type");
  }

  boost::algorithm::to_upper(name);
  std::map<std::string,sConnection>::iterator itName = connectionMap.find(name);
  if(itName != connectionMap.end()){
    throw std::runtime_error("Duplicate connection name");
  }
  printf("Creating connection %s(%s) with args:",name.c_str(),type.c_str());
  for(size_t i=0;i<args.size();i++){
    printf(" %s",args[i].c_str());
  }
  printf("\n");
  std::shared_ptr<Connection> (*fptr)(FUNCTION_PTR_SIGNATURE) = itType->second;
  std::shared_ptr<Connection> ret = (*fptr)(type,name,args);
  sConnection newConnection {type,ret};
  connectionMap[name]=newConnection;

  return ret;
}

std::shared_ptr<Connection> Factory::GetConnection(std::string name){
  boost::algorithm::to_upper(name);
  std::map<std::string,sConnection>::iterator itName = connectionMap.find(name);
  if(itName != connectionMap.end()){
    return itName->second.ptr;
  }

  throw std::runtime_error("Connection not found");      
  //never called
  return std::shared_ptr<Connection>();
}

bool Factory::DeleteConnection(std::string name){
  boost::algorithm::to_upper(name);
  std::map<std::string,sConnection>::iterator itName = connectionMap.find(name);
  if(itName != connectionMap.end()){
    connectionMap.erase(itName);
    return true;
  }

  return false;
}


std::vector<std::string> Factory::GetConnectionTypes(){
  std::vector<std::string> ret;
  for(auto it = connectionTypeMap.begin();
      it != connectionTypeMap.end();
      ++it){
    ret.push_back(it->first);
  }
  return ret;
}
