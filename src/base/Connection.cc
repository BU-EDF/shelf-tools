#include <base/Connection.hh>
#include <base/Factory.hh>

Connection::Connection(std::string const & _type,
		       std::string const & _name){
  type=_type;
  name=_name;
}

Connection::~Connection(){
}

void Connection::DoConnect(){}
void Connection::Disconnect(){}

void Connection::Setup(std::vector<std::string> const &){
  return;
}

std::string Connection::GetName(){
  return name;
}
std::string Connection::GetType(){
  return type;
}

