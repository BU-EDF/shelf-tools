#include <base/Sensor.hh>
#include <base/Factory.hh>
#include <base/net_helpers.hh>

#include <string.h>
#include <time.h>
#include <stdio.h>
#include <string>

#include <arpa/inet.h>
#include <sys/types.h>

#include <unistd.h>

#include <syslog.h>

#include <ctime> // for time()

Sensor::Sensor(std::string const & _type,
	       std::string const & _name)
{
  type=_type;
  name=_name;
}

Sensor::~Sensor(){
}

float Sensor::GetVal(){return -1;}

std::string Sensor::GetName(){
  return name;
}
std::string Sensor::GetType(){
  return type;
}

void Sensor::SetDBConnection(std::string const & name){
  //This will work or throw
  dbConn = std::static_pointer_cast<DBConnection>(GetParent()->GetConnection(name));
}

int Sensor::Report(){
//  /*CHECK IF databaseName.c_str() is empty (check std::string docs) ,return -1 if so*/
//  if (connDB->GetDatabaseName().empty()){
//    syslog(LOG_INFO,"database name is empty! Please update it before calling this function");
//    return -1;
//  }

  /* get float value for our particular sensor */
  float value = GetVal();

  if(!GetDBConnection()){
    throw std::runtime_error("Missing DB connection "+
			     GetName()+
			     "/"+
			     GetType());
  }

  // Get the database name for the entry 
  std::string databaseName = GetDBConnection()->GetDatabaseName();

  // Get timestamp
  time_t timeNow = time(NULL);

  // Report to Graphite DB
  GetDBConnection()->Report(value, timeNow, databaseName);
 
  return 1;
}


Factory * Sensor::GetParent(){
  return Factory::Instance();
}
