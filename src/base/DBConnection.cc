#include <base/DBConnection.hh>
#include <base/Factory.hh>

DBConnection::DBConnection(std::string const & _type,
			   std::string const & _name):
  Connection(_type,_name){
}

DBConnection::~DBConnection(){
  Disconnect();
}

void DBConnection::DoConnect(){}
void DBConnection::Disconnect(){}

void DBConnection::Setup(std::vector<std::string> const &){
  return;
}

void DBConnection::Report(float,time_t time,std::string){}

void DBConnection::SetDatabaseName(std::string const & name) {
  databaseName = name;
}
std::string DBConnection::GetDatabaseName(){
  return databaseName;
}
