#include <iostream>

#include <base/Factory.hh>
#include <base/Sensor.hh>
#include <base/Connection.hh>

#include <dlfcn.h>  //for dlopen

#include <unistd.h>
#include <string>
#include <fstream>
#include <boost/program_options.hpp>
#include <stdexcept>
#include <base/daemon.hh>
#include <signal.h>
#include <syslog.h>  

#include <sys/select.h>
#include <sys/types.h>
#include <sys/inotify.h>
#include <limits.h>
#include <sys/time.h>

namespace po = boost::program_options;

#define DevFac Factory::Instance()

std::vector<std::string> split_sensor_string(std::string sensor, std::string const & delimiter);

void LogRegisteredTypes(){
  std::string message("Registered Connection Types:\n");
  std::vector<std::string> types = DevFac->GetConnectionTypes();
  for(auto it = types.begin();it != types.end();it++){
    message.append("\t");
    message.append(*it);
    message.append("\n");
  }
  syslog(LOG_INFO,"%s",message.c_str());  

  message = "Registered Sensor Types:\n";
  types = DevFac->GetSensorTypes();
  for(auto it = types.begin();it != types.end();it++){
    message.append("\t");
    message.append(*it);
    message.append("\n");
  }
  syslog(LOG_INFO,"%s",message.c_str());  
}

void resetFDSets(fd_set * readFD, fd_set * writeFD, fd_set * exceptFD, std::vector<std::shared_ptr<Sensor> > sensorList, std::vector<std::shared_ptr<Connection> > connectionList, int &maxfd){
  FD_ZERO(readFD);
  FD_ZERO(writeFD);
  FD_ZERO(exceptFD);
  maxfd = 0;
  for (size_t i = 0; i < sensorList.size(); i++ ) {
    sensorList[i]->SetupFDSets(maxfd, readFD, writeFD, exceptFD);
  }
  for (size_t i = 0; i < connectionList.size(); i++) {
    connectionList[i]->SetupFDSets(maxfd, readFD, writeFD, exceptFD);
  }
}

void updateTimeout(time_t next, time_t current, struct timespec &waitTime){
  float diff_time = float((next-current)*0.75);
  waitTime.tv_sec = (long) diff_time;
  waitTime.tv_nsec = (diff_time - waitTime.tv_sec) * 1000000000L;
}

void processFDFired(fd_set * readFD, fd_set * writeFD, fd_set * exceptFD, std::vector<std::shared_ptr<Sensor> > sensorList, std::vector<std::shared_ptr<Connection> > connectionList){
  for (size_t i = 0; i < sensorList.size(); i++ ) {
    sensorList[i]->ProcessFDSets(readFD, writeFD, exceptFD);
  }
  for (size_t i = 0; i < connectionList.size(); i++ ) {
    connectionList[i]->ProcessFDSets(readFD, writeFD, exceptFD);
  }
  return;
}

int main(int argc, char ** argv){

  //=============================================================================
  //CLI arguments
  //=============================================================================
  std::string configFile("/etc/graphite_monitor");
  std::string runPath("/opt/address_table");
  std::string pidFile("/var/run/graphite_monitor.pid");
  boost::program_options::options_description commandLineOptions{"Options"}; // for parsing command line
  commandLineOptions.add_options()
    ("help,h", "Help screen")
    ("stand_alone", "Do not daemonize this process")
    ("config_file", boost::program_options::value<std::string>(),"config file")
    ("run_path" ,boost::program_options::value<std::string>(), "run path")
    ("pid_file" ,boost::program_options::value<std::string>(), "pid file");
  boost::program_options::variables_map commandLineVM; // for parsing command line 
  try {
    // parse command line
    boost::program_options::store(boost::program_options::parse_command_line(argc,
									     argv,
									     commandLineOptions),
				  commandLineVM);
  } catch(const boost::program_options::error &ex) {
    fprintf(stderr, "Caught exception while parsing command line: %s \nTerminating <%s>\n", ex.what(),argv[0]);       
    return -1;
  }
 
  if(commandLineVM.count("help")){
    std::cout << commandLineOptions << std::endl;
    return 0;
  }

  //check for CLI arguments
  if(commandLineVM.count("config_file")) {
    configFile = commandLineVM["config_file"].as<std::string>();
  }
  if(commandLineVM.count("run_path")) {
    runPath = commandLineVM["run_path"].as<std::string>();
  }
  if(commandLineVM.count("pid_file")) {
    pidFile = commandLineVM["pid_file"].as<std::string>();
  }  


  //=============================================================================
  //Setup the daemonization code
  //=============================================================================
  Daemon daemon;
  if(commandLineVM.count("stand_alone") == 0){
    printf("Daemonizing this process\n");
    daemon.daemonizeThisProgram(pidFile, runPath);
  }else{
    printf("Not Daemonizing this process\n");
  }
  unsigned int secondsToSleep = 30;
  syslog(LOG_INFO,"Using config file: %s\n",configFile.c_str());
  syslog(LOG_INFO,"Using run path: %s\n",runPath.c_str());
  syslog(LOG_INFO,"Using pid file: %s\n",pidFile.c_str());

  //=============================================================================
  //Open config file
  //=============================================================================  
  int numConfigOptions = -1;
  //  
  po::options_description fileOptions{"File"};
  try {
    fileOptions.add_options()
      ("plugin", po::value<std::string>(), "value")
      ("connection", po::value<std::string>(), "value")
      ("sensor", po::value<std::string>(), "value")
      ("poll_period", po::value<std::string>(), "value");
  } catch(const po::error &ex){
    syslog(LOG_ERR,"Config parsing caused exception: %s\n",ex.what());
    return -1;
  }

  std::ifstream ifs(configFile.c_str());
  //  if(ifs){
  po::parsed_options config_options =  po::parse_config_file(ifs, fileOptions);
  numConfigOptions = config_options.options.size();
  //}
  

  //Report to sys log which Connection and Sensor types are registerd
  LogRegisteredTypes();

  //=============================================================================
  //Load all the settings in the config file
  //=============================================================================  
  for ( int iOption = 0; iOption < numConfigOptions; iOption++ ) {
    std::string option = config_options.options[iOption].string_key;	
    if(option == "poll_time"){
      secondsToSleep = atoi(config_options.options[iOption].value[0].c_str());
    }
  }

  //=============================================================================
  //Find all the plugins in the config file
  //=============================================================================  
  for ( int iOption = 0; iOption < numConfigOptions; iOption++ ) {
    std::string option = config_options.options[iOption].string_key;	
    if(option == "plugin"){
      // values in config file are space-delimited
      std::string delimiter = " ";
      std::vector<std::string> optionsInfo;
      optionsInfo = split_sensor_string(config_options.options[iOption].value[0],delimiter);     
      if(optionsInfo.size() < 1){
	syslog(LOG_INFO,"Plugin incomplete: %s",config_options.options[iOption].value[0].c_str());
	continue;
      }
      void * lib_handle = NULL;
      lib_handle = dlopen(optionsInfo[0].c_str(),RTLD_NOW);
      if(lib_handle == NULL){
	char * errorString = dlerror();
	if(errorString != NULL){
	  syslog(LOG_INFO,"Error: %s\n",errorString);
	} else {
	  syslog(LOG_INFO,"Loaded plugin: %s\n",optionsInfo[0].c_str());
	}
      }else{
	syslog(LOG_INFO,"Loaded plugin: %s",config_options.options[iOption].value[0].c_str());
      }
    }
  }
  //=============================================================================
  //Find all the Connections in the config file
  //=============================================================================  
  std::vector<std::shared_ptr<Connection> > connections;
  for ( int iOption = 0; iOption < numConfigOptions; iOption++ ) {
    std::string option = config_options.options[iOption].string_key;	
    if(option == "connection"){
      // values in config file are space-delimited
      std::string delimiter = " ";
      std::vector<std::string> optionsInfo;
      optionsInfo = split_sensor_string(config_options.options[iOption].value[0],delimiter);     
      syslog(LOG_INFO,"Connection OptionsInfo size: %zu\n",optionsInfo.size());
      if(optionsInfo.size() < 2){
	syslog(LOG_INFO,"Connection incomplete: %s",config_options.options[iOption].value[0].c_str());
	continue;
      }
      //Get the name and type of this Connection
      std::string type = optionsInfo[0];
      optionsInfo.erase(optionsInfo.begin()); //remove the type
      std::string name = optionsInfo[0];
      optionsInfo.erase(optionsInfo.begin()); //remove the name
      
      try {
	std::shared_ptr<Connection> newConnection = DevFac->CreateConnection(type,name,optionsInfo);
  connections.push_back(newConnection);
	if(newConnection){		
	  syslog(LOG_INFO,"Adding connection: %s(%s)\n",name.c_str(),type.c_str());
	  for(size_t iOption = 0; iOption < optionsInfo.size();iOption++){
	    syslog(LOG_INFO,"    %s\n",optionsInfo[iOption].c_str());
	  }

	}
      } catch (std::exception &e){
	syslog(LOG_INFO,"%s",e.what());
	return 1;
      }
    }
  }

  //=============================================================================
  //Find all the Sensors in the config file
  //=============================================================================  
  std::vector<std::shared_ptr<Sensor> > sensors;
  for ( int iOption = 0; iOption < numConfigOptions; iOption++ ) {
    std::string option = config_options.options[iOption].string_key;	
    if(option == "sensor"){
      // values in config file are space-delimited
      std::string delimiter = " ";
      std::vector<std::string> optionsInfo;
      optionsInfo = split_sensor_string(config_options.options[iOption].value[0],delimiter);     
      if(optionsInfo.size() < 2){
	syslog(LOG_INFO,"Sensor incomplete: %s",config_options.options[iOption].value[0].c_str());
	continue;
      }
      //Get the name and type of this Connection
      std::string type = optionsInfo[0];
      optionsInfo.erase(optionsInfo.begin()); //remove the type
      std::string name = optionsInfo[0];
      optionsInfo.erase(optionsInfo.begin()); //remove the name
      try {	
	std::shared_ptr<Sensor> newSensor = DevFac->CreateSensor(type,name,optionsInfo);
	if(newSensor){	
	  sensors.push_back(newSensor);
	  syslog(LOG_INFO,"Adding sensor: %s(%s)\n",name.c_str(),type.c_str());
	  for(size_t iOption = 0; iOption < optionsInfo.size();iOption++){
	    syslog(LOG_INFO,"    %s\n",optionsInfo[iOption].c_str());
	  }
	}
      } catch (std::exception &e){
	syslog(LOG_INFO,"%s",e.what());
      }
    }
  }
    
  // ============================================================================
  // Daemonize this process
  struct sigaction sa_INT,sa_TERM,old_sa;
  daemon.changeSignal(&sa_INT , &old_sa, SIGINT);
  daemon.changeSignal(&sa_TERM, NULL   , SIGTERM);
  daemon.SetLoop(true);
  
  
  int attempts = 0;
  int successful = 0;
  int attemptsTimer = 0;
  // sleep and repeat this process
  time_t currentTime,nextTime;
  currentTime = time(NULL);
  nextTime = currentTime;
  //Inotify fd_set and timing initialization
  fd_set read_fd_set;
  fd_set write_fd_set;
  fd_set except_fd_set;
  int maxfd;
  int retVal;
  struct timespec timeout;
  timeout.tv_nsec = 0;
  while(daemon.GetLoop()){    
    if(currentTime >= nextTime){
      while(currentTime >= nextTime){
        nextTime+=secondsToSleep;
      }
      for (size_t i = 0; i < sensors.size(); i++ ) {
	        attempts++;
	        try{
	          sensors[i]->Report();
	          successful++;
	        }catch(const std::exception & e){
	          syslog(LOG_INFO,"Exception %s\n",e.what());
	        }
      }
      attemptsTimer += secondsToSleep;
      if ( attemptsTimer >= 3600){
	        syslog(LOG_INFO, "%d out of %d successful reports in the last 10 minutes\n", successful, attempts);
	        attempts = 0;
	        successful = 0;
	        attemptsTimer = 0;
      }
    }else{
      resetFDSets(&read_fd_set, &write_fd_set, &except_fd_set, sensors, connections, maxfd);
      updateTimeout(nextTime, currentTime, timeout);
      retVal = pselect(maxfd+1, &read_fd_set, &write_fd_set, &except_fd_set, &timeout, NULL);
      if (retVal > 0){
        processFDFired(&read_fd_set, &write_fd_set, &except_fd_set, sensors, connections);
      }else if (retVal < 0){
	syslog(LOG_WARNING,"Got errno %d: %s\n",errno,strerror(errno));
      }
    }
    currentTime = time(NULL);
  }

  // Restore old action of receiving SIGINT (which is to kill program) before returning 
  sigaction(SIGINT, &old_sa, NULL);
  syslog(LOG_INFO,"Monitor Daemon ended\n");

  return 0;
}


std::vector<std::string> split_sensor_string(std::string sensor, std::string const & delimiter){
  
  size_t position = 0;
  std::string token;
  std::vector<std::string> sensor_info;
  while( (position = sensor.find(delimiter)) != std::string::npos) {    
    token = sensor.substr(0, position);
    if (!token.empty()){
      sensor_info.push_back(token);
    }
    sensor.erase(0, position+delimiter.length());
  }
  if (!sensor.empty()){
    sensor_info.push_back(sensor);
  }

  return sensor_info;
}
