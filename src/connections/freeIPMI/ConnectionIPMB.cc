#include <connections/freeIPMI/ConnectionIPMB.hh>
#include <string.h>
#include <freeipmi/driver/ipmi-openipmi-driver.h>
//register the connection
RegisterConnection(ConnectionIPMB,"ConnectionIPMB")

ConnectionIPMB::ConnectionIPMB(std::string const & type,
			       std::string const & name):
  Connection(type,name),
  ipmiContext(NULL),
  setup(false),
  hostname(""),
  username(""),
  password(""){
}
ConnectionIPMB::~ConnectionIPMB(){
  // close connection
  Disconnect();
}
void ConnectionIPMB::Setup(std::vector<std::string> const & args){
  if(args.size() < 1){
    throw std::runtime_error("Too few arguments to ConnectionIPMB constructor!\n");
  }
  hostname = args[0];
  if(args.size() > 1){
    username = args[1];
  }
  if(args.size() > 2){
    password = args[2];
  }
  setup = true;  
}


void ConnectionIPMB::DoConnect(){
  if(!setup){
    throw std::runtime_error("IPMB Connection parameters not set!\n");
  }
  if(ipmiContext == NULL){
    ipmiContext = ipmi_ctx_create();
    int openRet = ipmi_ctx_open_outofband(ipmiContext,
					  hostname.c_str(),  //shelf hostname
					  //					  username.c_str(),  //username
					  //					  password.c_str(),  //password
					  "","",
					  IPMI_AUTHENTICATION_TYPE_NONE,  //authentication type
					  IPMI_PRIVILEGE_LEVEL_ADMIN,     //privilege level
					  0,    // session timeout (0 => default)
					  0,    // retransmit timeout (0 => default)
					  0,    // workaround flags
					  0);//IPMI_FLAGS_DEBUG_DUMP);   //flags
    if(openRet < 0){
      Disconnect();
      throw std::runtime_error(ipmi_openipmi_ctx_strerror(ipmi_ctx_errnum(ipmiContext)));
    }
  }
}


void ConnectionIPMB::Disconnect(){
  if(ipmiContext != NULL){
    ipmi_ctx_close(ipmiContext);
    ipmiContext = NULL;
  }
}


int ConnectionIPMB::rawCommand(uint8_t channelNumber,  // 0
			       uint8_t ipmbAddress,    //device address
			       uint8_t lun,            // 0 
			       uint8_t netFn,         // page 67 of ipmi doc.  0x4 is sensor/event
			       const void *requestBuffer,
			       unsigned int requestBufferSize, 
			       void *responseBuffer,
			       unsigned int responseBufferSize){
  int ret =0;
  int tries = 2;
  do{
    if(ipmiContext == NULL){
      DoConnect();
    }else{
      break;
    }
    tries--;
  }while(tries > 0);
  if(tries <= 0){
    throw std::runtime_error("Bad ipmiContext "+
			     GetName()+
			     "/"+
			     GetType());
  }


  tries = 2;
  do{
    ret = ipmi_cmd_raw_ipmb ( ipmiContext,
			      channelNumber,
			      ipmbAddress,
			      lun,
			      netFn,
			      requestBuffer,requestBufferSize,
			      responseBuffer,responseBufferSize);        
    if(ret == -1) {
      switch (ipmi_ctx_errnum(ipmiContext)) {
	case IPMI_ERR_SUCCESS:
	  fprintf(stderr,"Got IPMI_ERR_SUCCESS\n");
	  ret = 0;
	  tries=0;
	  break;
	case IPMI_ERR_COMMAND_INVALID_OR_UNSUPPORTED:

	  //fprintf(stderr,"Got IPMI_ERR_BAD_COMPLETION_CODE:\n");
	  tries=0;
	  break;

//	  fprintf(stderr,"Got IPMI_ERR_COMMAND_INVALID_OR_UNSUPPORTED\n");
//	  tries=0;
//	  break;
	case IPMI_ERR_BAD_COMPLETION_CODE:
	default:
	  //	  fprintf(stderr,"IPMI error #%d: %s\n",ipmi_ctx_errnum(ipmiContext),ipmi_ctx_strerror(ipmi_ctx_errnum(ipmiContext)));
	  Disconnect();
	  DoConnect();
	  tries--;
	  break;
	}
    }else{
      tries=0;
    }
  }while(tries > 0);

  //Check if the write eventually worked
  if(tries < 0){
    throw std::runtime_error("Connection failed "+
			     GetName()+
			     "/"+
			     GetType());
  }

  return ret;

}

void ConnectionIPMB::SetupFDSets(int &maxfd, fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){
  return; 
}

void ConnectionIPMB::ProcessFDSets(fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){
  return;
}
