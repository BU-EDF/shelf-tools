#include <connections/ApolloSM/ConnectionApolloSM.hh>
#include <sys/inotify.h>
#include <syslog.h>

#include <boost/filesystem.hpp>
#include <syslog.h>  

//register the connection
RegisterConnection(ConnectionApolloSM,"ConnectionApolloSM")

ConnectionApolloSM::ConnectionApolloSM(std::string const & type,
				       std::string const & name
				       ):
Connection(type,name){
  
}
void ConnectionApolloSM::Setup(std::vector<std::string> const & args){
  /*
    args:
    [0] connectionFile,
  */
  if(args.size() < 1){
    throw std::runtime_error("Too few ConnectionApolloSM args:\n "\
			     "args:\n"              \
			     " [0] connectionFile\n" 
			     );
  }
  connectionFile=args[0];
  boost::filesystem::path conn_path{connectionFile};
  watch_dir = conn_path.parent_path().string();
  watch_file = conn_path.filename().string();


  fd = inotify_init();
  if (fd < 0){
    printf("Failed to init inotify\n");
    throw std::runtime_error("Failed to init inotify\n");
  }
  
  wd = inotify_add_watch(fd, watch_dir.c_str(),
			 IN_DELETE | IN_CLOSE_WRITE );
  if (wd == -1)
    {
      printf("Couldn't add watch to %s\n", watch_dir.c_str());
      throw std::runtime_error("Couldn't add watch to "+ watch_dir);
    }
  else
    {
      printf("Watching:: %s for changes to %s\n", watch_dir.c_str(),watch_file.c_str());
    }
  DoConnect();
}

ConnectionApolloSM::~ConnectionApolloSM(){
  Disconnect();
}

void ConnectionApolloSM::DoConnect(){
  if(SM.use_count() == 0){
    Disconnect();
  }

  std::vector<std::string> connArgs;
  connArgs.push_back(connectionFile);
  //Create and initialize ApolloSM
  SM = std::make_shared<ApolloSM>(connArgs);
  ApolloSMTimestamp = time(NULL);  
}

void ConnectionApolloSM::Disconnect(){
  SM.reset();
}

void ConnectionApolloSM::SetupFDSets(int &maxfd, fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){
  FD_SET(fd, readFD);
  if (maxfd < fd){
    maxfd = fd;
  }
  return; 
}

size_t const inotifyEventSize = 2048;
uint8_t inotifyEvent[inotifyEventSize+1];
static struct inotify_event * readINotifyMessage(int fd){
  //Assumes this has atleast one message in it
  struct inotify_event * event = (struct inotify_event *) inotifyEvent;
  memset(event,0,inotifyEventSize);
  //get struct
  size_t readSize = read(fd,event,inotifyEventSize);
  if(readSize == size_t(-1)){
    fprintf(stderr,"Error %d(%s)\n",errno,strerror(errno));
  }else if(readSize < sizeof(struct inotify_event)){
    fprintf(stderr,"Bad read for inotify event %zu vs %zu\n",readSize,sizeof(struct inotify_event));
    throw std::system_error(errno, std::generic_category(),"inotify event read");
  }
  return event;
}

static bool address_table_changed(int fd, std::string const & checkFilename) {
  bool changed = false;
  fd_set read_fd_set;
  FD_ZERO(&read_fd_set);
  FD_SET(fd, &read_fd_set);
  struct timespec timeout;
  timeout.tv_sec = 0;
  timeout.tv_nsec = 0;
  int ret = pselect(fd+1, &read_fd_set, NULL, NULL, &timeout, NULL);
  if (ret == -1) {
    // fd likely invalid... difficult to recover from at this point
    throw std::system_error(errno, std::generic_category(),
                            "pselect returned error");
  }
  
  while (ret > 0){
    //read out the structs from inotify
    if(FD_ISSET(fd,&read_fd_set)){
      //This is from the inotify watch
      struct inotify_event const * event = readINotifyMessage(fd);
      std::string filename(event->name,event->len);
      //fprintf(stderr,"Mask: 0x%08X File: %s\n",event->mask,filename.c_str());
      if(std::string::npos != filename.find(checkFilename)){
	//This is a change in the filename we care about
	if(event->mask & IN_DELETE){
	  //the file was deleted, so a change is coming.  Wait until we get a creation
	  changed = true;
	}else if(event->mask & IN_CLOSE_WRITE){
	  // we are done! (TODO wait for the file to be closed?)
	  break;
	}	
      }else{
	//A different file
	if (!changed) {
	  //we haven't seen a delete for our checkFilename, so we can just leave this loop
	  //otherwise we stay in this loop looking for the create
	  break;
	}
      }
    }else{
      //not our FD
      break;
    }

    //re-run pselect
    FD_ZERO(&read_fd_set);
    FD_SET(fd, &read_fd_set);
    timeout.tv_sec = 1;
    timeout.tv_nsec = 0;
    ret = pselect(fd+1, &read_fd_set, NULL, NULL,&timeout, NULL);
    if (ret == -1) {
      // fd likely invalid... difficult to recover from at this point
      throw std::system_error(errno, std::generic_category(),
                            "pselect returned error");
    }
  }  
  return changed;
}

void ConnectionApolloSM::ProcessFDSets(fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){

  //This will block during an address table change. 
  if (address_table_changed(fd,watch_file)){    
    syslog(LOG_INFO,"Connections file has been updated\n");
    Disconnect();
    DoConnect();
  }
  return;
}

std::shared_ptr<ApolloSM> const &  ConnectionApolloSM::GetSM(){
  return SM;
}

time_t ConnectionApolloSM::getApolloSMTimestamp(){
  return ApolloSMTimestamp;
}
