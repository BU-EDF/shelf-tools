#include <connections/graphite/ConnectionGraphite.hh>
#include <strings.h> //for bzero

#include <arpa/inet.h> //for inet_pton

#include <unistd.h> //for close

#include <syslog.h> //for LOG_WARNING

#include <math.h> //for isnan()

//register the connection
RegisterConnection(ConnectionGraphite,"ConnectionGraphite")


ConnectionGraphite::ConnectionGraphite(std::string const & type,
				       std::string const & name
				       ):
DBConnection(type,name),
  port(-1),
  sockfd(-1),
  bufferSize(0),
  buffer(NULL){
}

void ConnectionGraphite::Setup(std::vector<std::string> const & args){
  if(args.size() < 3){
    throw std::runtime_error("Too few ConnectionGraphite args:\n"\
			     " [0] IP\n"\
			     " [1] port\n"\
			     " [2] database\n"
			     );      
  }
  IP = args[0];
  port = atoi(args[1].c_str());

  SetDatabaseName(args[2]);

  //Setup the sockaddr_in struct and validate the IP
  bzero(&servaddr, sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(port); /* 2003 is plaintext port */
  if (inet_pton(AF_INET, IP.c_str(), &servaddr.sin_addr) <= 0) {
    throw std::runtime_error("Invalid IP: "+IP+" in ConnectionGraphite \n");      
  }
  
}

ConnectionGraphite::~ConnectionGraphite(){
  Disconnect();
  if(bufferSize > 0 && buffer != NULL){
    delete [] buffer;
    buffer = NULL;
    bufferSize = 0;
  }
}

void ConnectionGraphite::DoConnect(){
  //Check if we are already connected
  if(sockfd >= 0){
    //connection already setup
    return;
  }else{
    //Create the socket
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {      
      throw std::runtime_error("Cannot create socket");      
    }
  }

  if (connect(sockfd, (struct sockaddr*) &servaddr, sizeof(servaddr)) < 0){
    close(sockfd);
    sockfd = -1;
    throw std::runtime_error("Unable to connect to: "+IP+" in ConnectionGraphite \n");      
  }
  syslog(LOG_INFO,"%s connected.",GetName().c_str());
}

void ConnectionGraphite::Disconnect(){
  if(sockfd >= 0){
    close(sockfd);
    sockfd = -1;
  }
}

void ConnectionGraphite::Report(float value, time_t timeNow,std::string databaseNameAppend=""){  

  if(sockfd < 0){
    DoConnect();
  }

  std::string _databaseName = databaseName + databaseNameAppend;
  
  /*CHECK IF _databaseName.c_str() is empty (check std::string docs) ,return -1 if so*/
  if (_databaseName.empty()){
    throw std::runtime_error("Empty database name in "+
			     GetName()+
			     "/"+
			     GetType());
  }

  //If the sensor time isn't specified, find it ourselves
  if(!timeNow){
    timeNow = time(NULL);
  }
  
  
  //Generate the string to write (we will expand it if it needs)
  size_t neededBufferSize;
  int tries = 2;
  do{
    //write the string for our message into the buffer   
    if(!isnan(value)){
      neededBufferSize = snprintf(buffer,bufferSize,
				  "%s %f %ld\n",
				  (char const *) _databaseName.c_str(),
				  value,
				  timeNow);
    }else{
      neededBufferSize = snprintf(buffer,bufferSize,
				  "%s None %ld\n",
				  (char const *) _databaseName.c_str(),
				  timeNow);      
    }
    //neededBufferSize++; //for null
    
    //Check if the buffer was big enough
    if(neededBufferSize > bufferSize){
      if(buffer != NULL){
	//delete old buffer
	delete [] buffer;
      }
      //update the buffer size
      bufferSize = neededBufferSize+1 ;
      //create the new buffer
      buffer = new char[bufferSize+1];//+1 for NULL
      bzero(buffer,bufferSize+1);
      tries--;
    }else{
      //things were ok, so we are done
      break;
    }
  }while(tries > 0);

  //Die if we couldn't allocate a big enough buffer
  if(tries <= 0){
    throw std::runtime_error("Unable to allocate a big enough buffer in "+
			     GetName()+
			     "/"+
			     GetType());
  }


  // write the result
  size_t writeReturn;
  tries=2;
  do{
    writeReturn = writen(sockfd, buffer, neededBufferSize);
    if(writeReturn < 0){
      syslog(LOG_WARNING,"Sensor write failed. Reconnecting.");
      //Try re-connecting and re-writing
      Disconnect();
      DoConnect();
      tries--;
    }else{
      break;
    }
  }while(tries > 0);

  //Check if the write eventually worked
  if(writeReturn < 0){
    throw std::runtime_error("Connection failed "+
			     GetName()+
			     "/"+    
			     GetType());
  }
}


void ConnectionGraphite::SetDatabaseName(std::string const & name){
  databaseName = name;
}
std::string ConnectionGraphite::GetDatabaseName(){
  return databaseName;
}

void ConnectionGraphite::SetupFDSets(int &maxfd, fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){
  return; 
}

void ConnectionGraphite::ProcessFDSets(fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){
  return;
}