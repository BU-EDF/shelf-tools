#include <connections/example/ConnectionDummyA.hh>


#include <syslog.h>


//register the connection
RegisterConnection(ConnectionDummyA,"ConnectionDummyA")

ConnectionDummyA::ConnectionDummyA(std::string const & type,
				       std::string const & name
				       ):
Connection(type,name){
  
}
void ConnectionDummyA::Setup(std::vector<std::string> const & args){
  if(args.size() < 1){
    throw std::runtime_error("Too few ConnectionDummyA args:\n "\
			     "args:\n"              \
			     " [0] watchDirectory\n"
			     );
  }  
  std::string watchDirectory=args[0];
  fd = inotify_init();
  wd = inotify_add_watch(fd, watchDirectory.c_str(), IN_CREATE | IN_MODIFY | IN_DELETE);
  if (wd == -1)
    {
      printf("Couldn't add watch to %s\n", watchDirectory.c_str());
    }
  else
    {
      printf("Watching:: %s\n", watchDirectory.c_str());
    }
}

ConnectionDummyA::~ConnectionDummyA(){
}

void ConnectionDummyA::SetupFDSets(int &maxfd, fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){
  FD_SET(fd, readFD);
  FD_SET(fd, writeFD);
  FD_SET(fd, exceptFD);
  if (maxfd < fd){
    maxfd = fd;
  }
  return; 
}

void ConnectionDummyA::ProcessFDSets(fd_set * readFD, fd_set * writeFD, fd_set * exceptFD){
  if (FD_ISSET(fd, readFD) == 1 || FD_ISSET(fd, writeFD) == 1 || FD_ISSET(fd, exceptFD) == 1){
    return;
  }
  return;
}
