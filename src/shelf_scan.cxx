#include <stdio.h>

#include <base/Factory.hh>
#include <base/Connection.hh>
#include <sensors/atca/FRUReader.hh>
#include <connections/freeIPMI/ConnectionIPMB.hh>


#include <boost/program_options.hpp>
#include <helpers/optionParsing.hh>
#include <stdexcept>
#include <string>
#include <iostream>
#include <fstream>
#include <memory> //for smart pointer


namespace po = boost::program_options;

#define DevFac Factory::Instance()
std::vector<std::string> split_sensor_string(std::string sensor, std::string const & delimiter);

std::vector<std::string> split_sensor_string(std::string sensor, std::string const & delimiter){
  
  size_t position = 0;
  std::string token;
  std::vector<std::string> sensor_info;
  while( (position = sensor.find(delimiter)) != std::string::npos) {
    token = sensor.substr(0, position);
    sensor_info.push_back(token);
    sensor.erase(0, position+delimiter.length());
  }
  sensor_info.push_back(sensor);

  return sensor_info;
}

char GetSpinnyThing(){
  static char spinnyThing[] = {'-','\\','|','/'};
  static int sel;
  //cycle through the 4 options
  sel++;
  sel &= 0x3;
  return spinnyThing[sel];
}

int main(int argc, char ** argv){
  po::options_description cli_options("Shelf Scan Options");
  cli_options.add_options()
    ("help,h", "Help screen")
    ("connection,c", po::value<std::string>(), "name IP")
    ("config_file",          po::value<std::string>(), "config file"); // This is the only option not also in the file option (obviously); 
   
  po::options_description cfg_options("SM_boot options");
  cfg_options.add_options()
    ("connection,c", po::value<std::string>(), "name IP");

  std::map<std::string,std::vector<std::string> > allOptions;

  //Do a quick search of the command line only to look for a new config file.
  //Get options from command line,
  try { 
    FillOptions(parse_command_line(argc, argv, cli_options),
    allOptions);
  } catch (std::exception &e) {
    fprintf(stderr, "Error in BOOST parse_command_line: %s\n", e.what());
    return 0;
  }
  //Help option - ends program 
  if(allOptions.find("help") != allOptions.end()){
    std::cout << cli_options << '\n';
    return 0;
  }  

  //Determine a config file name
  std::string configFileName = GetFinalParameterValue(std::string("config_file"),allOptions,std::string("/etc/shelf_scan"));
  //Get options from config file
  std::ifstream configFile(configFileName.c_str());   
  if(configFile){
    try { 
      FillOptions(parse_config_file(configFile,cfg_options,true),
      allOptions);
    } catch (std::exception &e) {
      fprintf(stderr, "Error in BOOST parse_config_file: %s\n", e.what());
    }
    configFile.close();
  }



  
  //Connect to all the shelves specified
  std::vector<std::shared_ptr<ConnectionIPMB> > shelf;
  {
    auto connections = allOptions["connection"];
    for(auto itConnection = connections.begin();
	itConnection != connections.end();
	itConnection++){

      // values in config file are space-delimited
      std::string delimiter = " ";
      std::vector<std::string> optionsInfo;
      optionsInfo = split_sensor_string(*itConnection,delimiter);     
      //Setup any missing arguments

      if(optionsInfo.size() == 1){
	//We have only one argument, assume it is the IP and make the name the IP
	optionsInfo.push_back(optionsInfo[0]);	
      }
      if(optionsInfo.size() == 2){
	//We have two arguments, assume it is the name and IP and add the type
	optionsInfo.insert(optionsInfo.begin(),"ConnectionIPMB");
      }
      //ignore aguments past 3

      
      //Get the name and type of this Connection
      std::string type = optionsInfo[0];
      optionsInfo.erase(optionsInfo.begin()); //remove the type
      std::string name = optionsInfo[0];
      optionsInfo.erase(optionsInfo.begin()); //remove the name
      
      try {
	std::shared_ptr<ConnectionIPMB> newConnection = std::dynamic_pointer_cast<ConnectionIPMB>(DevFac->CreateConnection(type,name,optionsInfo));
	if(newConnection){		
	  printf("Adding connection: %s(%s)\n",name.c_str(),type.c_str());
	  shelf.push_back(newConnection);
	}
      } catch (std::exception &e){
	printf("%s",e.what());
      }
    }
  }

  
  
  const size_t dotCountMax = 80;

  // net_fn code for "Application" 0x06
  uint8_t net_fn = 0x06;
  const size_t buf_rs_size = 256;
  uint8_t buf_rs[buf_rs_size];
  // it's necessary to send 2 raw bytes to get our sensor data back
  const size_t buf_rq_size = 1;
  uint8_t buf_rq[buf_rq_size];
  // 0x01 is the ipmi raw command for get device ID
  buf_rq[0] = 0x01;


  for(auto itShelf = shelf.begin();
      itShelf != shelf.end();
      ++itShelf){
    printf("Searching %s...\n[",(*itShelf)->GetName().c_str());
    for(size_t iDot = 0;iDot < 0x7F/(0x7F/dotCountMax);iDot++){
      printf("=");
    }
    printf("]\n[.");
    size_t deltaDot = 0x7F/dotCountMax;
    size_t nextDot = deltaDot;

    std::vector<FRUReader> FRUs;
    for(uint8_t device = 0x0;
	device <= 0x7F;
	device++){
      int rawRet = (*itShelf)->rawCommand(0,
				       device<<1,
				       0,
				       net_fn,
				       (void const *) buf_rq, buf_rq_size,
				       buf_rs, buf_rs_size);
      if(rawRet >= 0){
	try{
	  FRUs.push_back(FRUReader((*itShelf)->GetName(),device<<1,0));
	}catch(std::exception &e){
	  printf("%s\n",e.what());
	}
      }
      if(device > nextDot){
	printf(".");
	fflush(stdout);
	nextDot+=deltaDot;
      }
    }
    printf("]\n");
    printf("IPMB(FRU) ");
    printf("==========\n");
    for(size_t iFRU = 0; iFRU < FRUs.size();iFRU++){
      time_t temp = FRUs[iFRU].GetBoardMfgDate();
      printf("0x%02X:\n"\
	     "      %s"\
	     "      \"%s\"\n"\
	     "      \"%s\"\n"\
	     "      \"%s\"\n"\
	     "      \"%s\"\n"\
	     "      \"%s\"\n"\
	     "      \"%s\"\n"\
	     "      \"%s\"\n"\
	     "      \"%s\"\n"\
	     "      \"%s\"\n",
	     FRUs[iFRU].GetAddr(),
	     asctime(gmtime(&temp)),
	     FRUs[iFRU].GetBoardManufacturerName().c_str(),
	     FRUs[iFRU].GetBoardProductName().c_str(),
	     FRUs[iFRU].GetBoardSerialNumber().c_str(),
	     FRUs[iFRU].GetBoardPartNumber().c_str(),
	     FRUs[iFRU].GetProductManufacturerName().c_str(),
	     FRUs[iFRU].GetProductName().c_str(),            
	     FRUs[iFRU].GetProductPartNumber().c_str(),      
	     FRUs[iFRU].GetProductVersionNumber().c_str(),   
	     FRUs[iFRU].GetProductSerialNumber().c_str()    
  	     );
    }    
  }

	
  return 0;
  
}
