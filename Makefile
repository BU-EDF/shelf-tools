
CXX=g++
#CXX=clang++
SRC=src
BUILD=build
BIN=bin
LIB=lib
INSTALL_PATH?=/opt/BUMonitor

CXX_FLAGS=-Iinclude -std=c++11 -fPIC -Wall -g -O3 -Werror -Wpedantic
LD_FLAGS=-L$(LIB) -lboost_program_options -lboost_system -Wl,-rpath=$(INSTALL_PATH)/lib


#================================================================================
#== LIB_BU_MONITOR
#================================================================================
LIB_BU_MONITOR_LIBS=-ldl
LIB_BU_MONITOR_SRC=$(wildcard $(SRC)/base/*.cc)
LIB_BU_MONITOR_SRC+=$(wildcard $(SRC)/helpers/*.cc)
LIB_BU_MONITOR_OBJ=$(patsubst $(SRC)/%.cc,$(BUILD)/%.o,$(LIB_BU_MONITOR_SRC))
LIB_BU_MONITOR=$(LIB)/libBUMonitor.so
LIB_BU_MONITOR_LD_FLAGS=-shared -fPIC -Wall -g -O3 -rdynamic


#================================================================================
#== BU_MONITOR
#================================================================================
BU_MONITOR_LIBS=-ldl -lBUMonitor
BU_MONITOR_SRC=$(SRC)/BUMonitor.cxx
BU_MONITOR_OBJ=$(patsubst $(SRC)/%.cxx,$(BUILD)/%.o,$(BU_MONITOR_SRC))
BU_MONITOR=$(BIN)/BUMonitor
BU_MONITOR_LD_FLAGS=-Wl,--no-as-needed

#================================================================================
#== GRAPHITE_CONNECTION_LIB
#================================================================================
LIB_BUMONITOR_GRAPHITE_CONN=$(LIB)/libBUMonitor_graphite.so
LIB_BUMONITOR_GRAPHITE_CONN_LIBS=-ldl -lBUMonitor
LIB_BUMONITOR_GRAPHITE_CONN_SRC=$(wildcard $(SRC)/connections/graphite/*.cc)
LIB_BUMONITOR_GRAPHITE_CONN_OBJ=$(patsubst $(SRC)/%.cc,$(BUILD)/%.o,$(LIB_BUMONITOR_GRAPHITE_CONN_SRC))
LIB_BUMONITOR_GRAPHITE_CONN_LD_FLAGS=-shared -fPIC -Wall -g -O3 -rdynamic

#================================================================================
#== FreeIPMI based Sensor
#================================================================================
LIB_BUMONITOR_ATCA=$(LIB)/libBUMonitor_ATCA.so
LIB_BUMONITOR_ATCA_SRC=$(wildcard $(SRC)/sensors/atca/*.cc)
LIB_BUMONITOR_ATCA_SRC+=$(wildcard $(SRC)/connections/freeIPMI/*.cc)
LIB_BUMONITOR_ATCA_OBJ=$(patsubst $(SRC)/%.cc,$(BUILD)/%.o,$(LIB_BUMONITOR_ATCA_SRC))
LIB_BUMONITOR_ATCA_LIBS=-lfreeipmi -lBUMonitor
LIB_BUMONITOR_ATCA_LD_FLAGS=-shared -fPIC -Wall -g -O3 -rdynamic

#================================================================================
#== Apoll Monitor Sensor
#================================================================================
CACTUS_ROOT?=/opt/cactus
APOLLO_PATH?=/opt/BUTool
LIB_APOLLO_MONITOR=$(LIB)/libBUMonitor_ApolloSM.so
LIB_APOLLO_MONITOR_SRC=$(wildcard $(SRC)/sensors/ApolloMonitor/*.cc)
LIB_APOLLO_MONITOR_SRC+=$(wildcard $(SRC)/connections/ApolloSM/*.cc)
LIB_APOLLO_MONITOR_OBJ=$(patsubst $(SRC)/%.cc,$(BUILD)/%.o,$(LIB_APOLLO_MONITOR_SRC))
LIB_APOLLO_MONITOR_LIBS= -lBUTool_Helpers \
			-lBUTool_ApolloSM \
			-lToolException \
			-lBUTool_IPBusIO \
                        -lBUMonitor
LIB_APOLLO_MONITOR_CXX_FLAGS=-I$(APOLLO_PATH)/include -I$(CACTUS_ROOT)/include 
LIB_APOLLO_MONITOR_LD_FLAGS=-L$(APOLLO_PATH)/lib -Wl,-rpath=$(APOLLO_PATH)/lib -shared -fPIC -Wall -g -O3 -rdynamic

#================================================================================
#== Apoll Monitor Sensor
#================================================================================
SHELF_SCAN_OBJ=$(BUILD)/shelf_scan.o 
SHELF_SCAN_LIBS=-lfreeipmi -lBUMonitor -lBUMonitor_ATCA
#================================================================================









build: $(BU_MONITOR)
.PHONEY: all clean distclean graphite_monitor shelf_scan lib_ApolloMonitor lib_ATCA list install
all: $(BU_MONITOR) shelf_scan $(LIB_APOLLO_MONITOR) $(LIB_BUMONITOR_ATCA) $(LIB_BUMONITOR_GRAPHITE_CONN)

#================================================================================
#== Aliases
#================================================================================
BUMonitor: $(BU_MONITOR)
shelf_scan: $(BIN)/shelf_scan
lib_ApolloMonitor: $(APOLLO_MONITOR_LIB)
lib_ATCA: $(LIB_BUMONITOR_ATCA)

#================================================================================
#== EXEs
#================================================================================
$(BU_MONITOR): $(BU_MONITOR_OBJ) $(LIB_BU_MONITOR)
	@mkdir -p $(dir $@)
	$(CXX) -o $@ $< $(LD_FLAGS) $(BU_MONITOR_LD_FLAGS) $(BU_MONITOR_LIBS)

$(BIN)/shelf_scan: $(SHELF_SCAN_OBJ) $(LIB_BU_MONITOR) $(LIB_BUMONITOR_ATCA)
	@mkdir -p $(dir $@)
	$(CXX) -o $@ $(SHELF_SCAN_OBJ) $(LD_FLAGS) $(SHELF_SCAN_LIBS)


#================================================================================
#== BUMONITOR LIB
#================================================================================
$(LIB_BU_MONITOR) : $(LIB_BU_MONITOR_OBJ) 
	@mkdir -p $(LIB)
	$(CXX) -o $@ $^ $(LD_FLAGS) $(LIB_BU_MONITOR_LD_FLAGS) $(INSTALL_LD_FLAGS) $(LIB_BU_MONITOR_LIBS) 

#================================================================================
#== Sensor Libs
#================================================================================
$(LIB_APOLLO_MONITOR) : $(LIB_APOLLO_MONITOR_OBJ) $(LIB_BU_MONITOR)
	@mkdir -p $(LIB)
	$(CXX) -o $@ $(LIB_APOLLO_MONITOR_OBJ) $(LD_FLAGS) $(LIB_APOLLO_MONITOR_LD_FLAGS) $(LIB_APOLLO_MONITOR_LIBS)

$(LIB_BUMONITOR_ATCA) : $(LIB_BUMONITOR_ATCA_OBJ) $(LIB_BU_MONITOR)
	@mkdir -p $(LIB)
	$(CXX) -o $@ $(LIB_BUMONITOR_ATCA_OBJ) $(LD_FLAGS) $(LIB_BUMONITOR_ATCA_LD_FLAGS) $(LIB_BUMONITOR_ATCA_LIBS)

$(LIB_BUMONITOR_GRAPHITE_CONN) : $(LIB_BUMONITOR_GRAPHITE_CONN_OBJ) $(LIB_BU_MONITOR)
	@mkdir -p $(LIB)
	$(CXX) -o $@ $(LIB_BUMONITOR_GRAPHITE_CONN_OBJ) $(LD_FLAGS) $(LIB_BUMONITOR_GRAPHITE_CONN_LD_FLAGS) $(LIB_BUMONITOR_GRAPHITE_CONN_LIBS)

#================================================================================
#== Generic rules
#================================================================================
#Build cc files
$(BUILD)/%.o: $(SRC)/%.cc
	@mkdir -p $(dir $@)
	$(CXX) -c -o $@ $< $(CXX_FLAGS)
#Build cxx files
$(BUILD)/%.o: $(SRC)/%.cxx
	@mkdir -p $(dir $@)
	$(CXX) -c -o $@ $< $(CXX_FLAGS)
#build ApolloMonitor cc files 
$(BUILD)/sensors/ApolloMonitor/%.o: $(SRC)/sensors/ApolloMonitor/%.cc
	@mkdir -p $(dir $@)
	$(CXX) -c -o $@ $< $(CXX_FLAGS) $(LIB_APOLLO_MONITOR_CXX_FLAGS)
#build ApolloMonitor cc files 
$(BUILD)/connections/ApolloSM/%.o: $(SRC)/connections/ApolloSM/%.cc
	@mkdir -p $(dir $@)
	$(CXX) -c -o $@ $< $(CXX_FLAGS) $(LIB_APOLLO_MONITOR_CXX_FLAGS)

#================================================================================
#== Install
#================================================================================

install: $(BU_MONITOR)
	rm -f $(BU_MONITOR)
	make $(BU_MONITOR) INSTALL_LD_FLAGS='-Wl,-rpath=$(INSTALL_PATH)/lib'
	install -m 775 -d $(INSTALL_PATH)/lib
	install -m 775 -d $(INSTALL_PATH)/bin
	install -b -m 775 $(BIN)/* ${INSTALL_PATH}/bin
	install -b -m 775 $(LIB)/* ${INSTALL_PATH}/lib

#================================================================================
#== Cleanup & helpers
#================================================================================
clean:
	rm -rf $(BUILD)
	rm -rf $(BIN)
	rm -rf $(LIB)

#list magic: https://stackoverflow.com/questions/4219255/how-do-you-get-the-list-of-targets-in-a-makefile                                                                
list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$' | column

