#
# spefile for atca shelf tools apollosm plugin
#
Name: %{name} 
Version: %{version} 
Release: %{release} 
Packager: %{packager}
Summary: BU ATCA shelf tools ApollSM plugin
License: Apache License
Group: BUTools
Source: git@gitlab.com:BU-EDF/shelf-tools.git
URL: git@gitlab.com:BU-EDF/shelf-tools.git
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot 
Prefix: %{_prefix}

%description
shelf tool graphite monitor apolloSM plugin

%prep

%build

%install 

# copy includes to RPM_BUILD_ROOT and set aliases
mkdir -p $RPM_BUILD_ROOT%{_prefix}
cp -rp %{sources_dir}/* $RPM_BUILD_ROOT%{_prefix}/.

#Change access rights
chmod -R 755 $RPM_BUILD_ROOT%{_prefix}/lib

%clean 

%post 

%postun 

%files 
%defattr(-, root, root) 
%{_prefix}/lib/*


